package com.app.rudo.network.api

import com.app.rudo.BuildConfig
import com.app.rudo.model.*
import com.app.rudo.model.ekeys.EAccessKeysList
import com.app.rudo.model.fingerprint.FingerprintList
import com.app.rudo.model.gateway.GatewayAccList
import com.app.rudo.model.iccard.IccardList
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.lockdetails.LockDetails
import com.app.rudo.model.locklist.GetLockList
import com.app.rudo.model.recodes.RecordListModel
import com.app.rudo.network.NetworkConnectionInterceptor
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Query


/**
 * Created by : Satyabrata Bhuyan on 20-09-2019.
 * Company : Yutu Electronics
 * Email : s.bhuyan@gmail.com
 */
interface RudoApi {

    @POST("/list/")
    @FormUrlEncoded
    fun userLogin(
        @Query("client_id") clientId: String?,
        @Field("client_secret") clientSecret: String?,
        @Field("grant_type") grantType: String?,
        @Field("username") username: String?,
        @Field("password") password: String?,
        @Field("redirect_uri") redirectUri: String?,
        @Field( "url", encoded = false) url: String?
    ): Deferred<Response<AuthResponse>>?

    @POST("/list/")
    @FormUrlEncoded
    fun forgotPassword(
        @Query("clientId") clientId: String?,
        @Field("clientSecret") clientSecret: String?,
        @Field("username") username: String?,
        @Field("password") password: String?,
        @Field("date") date: Long?,
        @Field("url", encoded = false) url: String?
    ): Deferred<Response<AuthResponse>>?

    @POST("/list/")
    @FormUrlEncoded
    fun userSignup(
        @Query("clientId") clientId: String?,
        @Field("clientSecret") clientSecret: String?,
        @Field("username") account: String?,
        @Field("password") password: String?,
        @Field("date") date: Long?,
        @Field("redirect_uri") redirectUri: String?,
        @Field("url") url: String?
    ):Deferred<Response<AuthResponse>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getLockList(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("pageNo") pageNo: Int,
        @Field("pageSize") pageSize: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ): Deferred<Response<GetLockList>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getLockDetails(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ): Deferred<Response<LockDetails>>?


    @POST("/list/")
    @FormUrlEncoded
    fun getKeyList(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ): Deferred<Response<KeyData>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getKeyDetails(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ): Deferred<Response<LockDetails>>?

    @POST("/list/")
    @FormUrlEncoded
    fun sendeAccessKey(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("receiverUsername") receiverUsername: String?,
        @Field("keyName") keyName: String?,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("remarks") remarks: String,
        @Field("remoteEnable") remoteEnable: Int,
        @Field("createUser") createUser: Int,
        @Field("date") currentTimeMillis: Long,
        @Field("url") url: String?
    ): Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun addPassCode(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("keyboardPwd") keyboardPwd: String?,
        @Field("keyboardPwdName") keyboardPwdName: String?,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("date") currentTimeMillis: Long,
        @Field("addType") addType: Int,//Adding method:1-via phone bluetooth, 2-via gateway, 3-via NB-IoT. The default value is 1 and should call SDK method to add passcode first. If you use the method 2, you can call this api directly.
        @Field("url") url: String?
    ): Deferred<Response<PasscodeModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getPassCode(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("keyboardPwdVersion") keyboardPwdVersion: Int?,
        @Field("keyboardPwdType") keyboardPwdType: Int?,
        @Field("keyboardPwdName") keyboardPwdName: String?,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("date") date: Long,
        @Field("url") url: String?
    ): Deferred<Response<PasscodeModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getLockKeyList(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("pageNo") pageNo: Int?,
        @Field("pageSize") pageSize: Int?,
        @Field("date") date: Long?,
        @Field("url") url: String?
    ): Deferred<Response<EAccessKeysList>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getPassCodeList(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("pageNo") pageNo: Int?,
        @Field("pageSize") pageSize: Int?,
        @Field("date") date: Long?,
        @Field("url") url: String?
    ): Deferred<Response<PasscodeMainModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getICCardList(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("pageNo") pageNo: Int?,
        @Field("pageSize") pageSize: Int?,
        @Field("date") date: Long?,
        @Field("url") url: String?
    ): Deferred<Response<IccardList>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getFingerprintList(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("pageNo") pageNo: Int?,
        @Field("pageSize") pageSize: Int?,
        @Field("date") date: Long?,
        @Field("url") url: String?
    ): Deferred<Response<FingerprintList>>?

    @POST("/list/")
    @FormUrlEncoded
    fun addICCard(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("cardNumber") cardNumber: String?,
        @Field("cardName") cardName: String?,
        @Field("startDate") startDate: Long?,
        @Field("endDate") endDate: Long?,
        @Field("addType") addType: Int?,//Adding method:1-via phone bluetooth, 2-via gateway, 3-via NB-IoT. The default value is 1 and should call SDK method to add passcode first. If you use the method 2, you can call this api directly.
        @Field("date") date: Long?,
        @Field("url") url: String?
    ): Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun addFingerprint(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("fingerprintNumber") fingerprintNumber: String?,
        @Field("fingerprintName") fingerprintName: String?,
        @Field("startDate") startDate: Long?,
        @Field("endDate") endDate: Long?,
        @Field("date") date: Long?,
        @Field("url") url: String?
    ): Deferred<Response<ApiResponseModel>>?


    @POST("/list/")
    @FormUrlEncoded
    fun lockInit(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockData") lockData: String?,
        @Field("lockAlias") alias: String?,
        @Field("date") date: Long,
        @Field("url") url: String?
    ): Deferred<Response<InitLockModel>>?


    @POST("/list/")
    @FormUrlEncoded
    fun unlockRecordsUpload(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("records") log: String?,
        @Field("date") date: Long,
        @Field("redirect_uri") redirectUri: String?,
        @Field("url") url: String?
    ): Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getUnlockRecords(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("pageNo") pageNo: Int,
        @Field("pageSize") pageSize: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ): Deferred<Response<RecordListModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun deletUser(
        @Query("clientId") clientId: String?,
        @Field("clientSecret") clientSecret: String?,
        @Field("username") username: String,
        @Field("date") date: Long,
        @Field("url") url: String?
    ): Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun clearIccard(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun clearFingerprint(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getGatewayAccountList(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("pageNo") pageNo: Int,
        @Field("pageSize") pageSize: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<GatewayAccList>>?


    @POST("/list/")
    @FormUrlEncoded
    fun deleteLock(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?


    @POST("/list/")
    @FormUrlEncoded
    fun deleteIcCard(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("cardId") cardId: Int,
        @Field("deleteType") deleteType: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun deleteFingerprint(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("fingerprintId") fingerprintId: Int,
        @Field("deleteType") deleteType: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?


    @POST("/list/")
    @FormUrlEncoded
    fun deleteeAccessKey(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("keyId") keyId: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun deleteePasscode(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("keyboardPwdId") keyboardPwdId: Int,
        @Field("deleteType") deleteType: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun authorizeEAccess(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("keyId") keyId: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun getEkeyList(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("pageNo") pageNo: Int,
        @Field("pageSize") pageSize: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<LockKeyListMode>>?

    @POST("/list/")
    @FormUrlEncoded
    fun addCustomPasscode(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("keyboardPwd") keyboardPwd: String,
        @Field("keyboardPwdName") keyboardPwdName: String?,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun uploadBatteryStatus(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("electricQuantity") electricQuantity: Int,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun uploadGatewayDetails(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("gatewayId") gatewayId: Int,
        @Field("modelNum") modelNum: String,
        @Field("hardwareRevision") hardwareRevision: String,
        @Field("firmwareRevision") firmwareRevision: String,
        @Field("networkName") networkName: String,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun initGateway(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("gatewayNetMac") gatewayNetMac: String?,
        @Field("date") date: Long,
        @Field("url") url: String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun lockViaGateway(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("date") date: Long,
        @Field("url") url:String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun unLockViaGateway(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("date") date: Long,
        @Field("url") url:String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list/")
    @FormUrlEncoded
    fun deleteGateway(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("gatewayId") gatewayId: Int?,
        @Field("date") date: Long,
        @Field("url") url:String?
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/list")
    @FormUrlEncoded
    fun getLockStatus(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("date") date: Long,
        @Field("url") url:String?
    ):Deferred<Response<LockStatus>>?

//-------------------------------------------------- Open TT Server -----------------------------------------------------------------------------

    @POST("/oauth2/token")
    @FormUrlEncoded
    fun userLoginWithOprnTTApi(
        @Query("client_id") clientId: String?,
        @Field("client_secret") clientSecret: String?,
        @Field("grant_type") grantType: String?,
        @Field("username") username: String?,
        @Field("password") password: String?,
        @Field("redirect_uri") redirectUri: String?
    ): Deferred<Response<AuthResponse>>?

    @POST("/v3/user/register")
    @FormUrlEncoded
    fun userSignupWithOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("clientSecret") clientSecret: String?,
        @Field("username") account: String?,
        @Field("password") password: String?,
        @Field("date") date: Long?,
        @Field("redirect_uri") redirectUri: String?
    ): Deferred<Response<AuthResponse>>?

    @POST("/v3/user/resetPassword")
    @FormUrlEncoded
    fun forgotPasswordWothOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("clientSecret") clientSecret: String?,
        @Field("username") username: String?,
        @Field("password") password: String?,
        @Field("date") date: Long?
    ): Deferred<Response<AuthResponse>>?
    @POST("/v3/lock/list")
    @FormUrlEncoded
    fun getLockListFromOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("pageNo") pageNo: Int,
        @Field("pageSize") pageSize: Int,
        @Field("date") date: Long
    ): Deferred<Response<GetLockList>>?

    @POST("/v3/lock/detail")
    @FormUrlEncoded
    fun getLockDetailsFromOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long
    ): Deferred<Response<LockDetails>>?


    @POST("/v3/key/get")
    @FormUrlEncoded
    fun getKeyListFromOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long
    ): Deferred<Response<KeyData>>?

    @POST("/v3/key/send")
    @FormUrlEncoded
    fun sendeAccessKeyToOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("receiverUsername") receiverUsername: String?,
        @Field("keyName") testkey: String?,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("remarks") remarks: String,
        @Field("remoteEnable") remoteEnable: Int,
        @Field("createUser") createUser: Int,
        @Field("date") currentTimeMillis: Long
    ): Deferred<Response<ApiResponseModel>>?

    @POST("/v3/keyboardPwd/add")
    @FormUrlEncoded
    fun addPassCodeOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("keyboardPwd") keyboardPwd: String?,
        @Field("keyboardPwdName") keyboardPwdName: String?,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("date") currentTimeMillis: Long,
        @Field("addType") addType: Int//Adding method:1-via phone bluetooth, 2-via gateway, 3-via NB-IoT. The default value is 1 and should call SDK method to add passcode first. If you use the method 2, you can call this api directly.

    ): Deferred<Response<PasscodeModel>>?

    @POST("/v3/keyboardPwd/get")
    @FormUrlEncoded
    fun getPassCodeOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("keyboardPwdVersion") keyboardPwdVersion: Int?,
        @Field("keyboardPwdType") keyboardPwdType: Int?,
        @Field("keyboardPwdName") keyboardPwdName: String?,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("date") date: Long?
    ): Deferred<Response<PasscodeModel>>?

    @POST("/v3/lock/listKey")
    @FormUrlEncoded
    fun getLockKeyListOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("pageNo") pageNo: Int?,
        @Field("pageSize") pageSize: Int?,
        @Field("date") date: Long?
    ): Deferred<Response<EAccessKeysList>>?


    @POST("/v3/lock/listKeyboardPwd")
    @FormUrlEncoded
    fun getPassCodeListOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("pageNo") pageNo: Int?,
        @Field("pageSize") pageSize: Int?,
        @Field("date") date: Long?
    ): Deferred<Response<PasscodeMainModel>>?

    @POST("/v3/identityCard/list")
    @FormUrlEncoded
    fun getICCardListOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("pageNo") pageNo: Int?,
        @Field("pageSize") pageSize: Int?,
        @Field("date") date: Long?
    ): Deferred<Response<IccardList>>?

    @POST("/v3/fingerprint/list")
    @FormUrlEncoded
    fun getFingerprintListOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("pageNo") pageNo: Int?,
        @Field("pageSize") pageSize: Int?,
        @Field("date") date: Long?
    ): Deferred<Response<FingerprintList>>?

    @POST("/v3/identityCard/add")
    @FormUrlEncoded
    fun addICCardOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("cardNumber") cardNumber: String?,
        @Field("cardName") cardName: String?,
        @Field("startDate") startDate: Long?,
        @Field("endDate") endDate: Long?,
        @Field("addType") addType: Int?,//Adding method:1-via phone bluetooth, 2-via gateway, 3-via NB-IoT. The default value is 1 and should call SDK method to add passcode first. If you use the method 2, you can call this api directly.
        @Field("date") date: Long?
    ):  Deferred<Response<ApiResponseModel>>?

    @POST("/v3/lock/initialize")
    @FormUrlEncoded
    fun lockInitOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockData") lockData: String?,
        @Field("lockAlias") alias: String?,
        @Field("date") date: Long
    ): Deferred<Response<InitLockModel>>?

    @POST("/v3/lockRecord/upload")
    @FormUrlEncoded
    fun unlockRecordsUploadOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("records") log: String?,
        @Field("date") l: Long
    ): Deferred<Response<ApiResponseModel>>?

    @POST("/v3/lockRecord/list")
    @FormUrlEncoded
    fun getUnlockRecordsOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("pageNo") pageNo: Int,
        @Field("pageSize") pageSize: Int,
        @Field("date") date: Long
    ): Deferred<Response<RecordListModel>>?


    @POST("/v3/fingerprint/add")
    @FormUrlEncoded
    fun addFingerprintOpenTTApi(
        @Field("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("fingerprintNumber") fingerprintNumber: String?,
        @Field("fingerprintName") fingerprintName: String?,
        @Field("startDate") startDate: Long?,
        @Field("endDate") endDate: Long?,
        @Field("date") date: Long?
    ): Deferred<Response<ApiResponseModel>>?

    @POST("/v3/user/delete")
    @FormUrlEncoded
    fun deletUserOpenTTApi(
        @Query("clientId") clientId: String?,
        @Field("clientSecret") clientSecret: String?,
        @Field("username") username: String,
        @Field("date") date: Long
    ): Deferred<Response<ApiResponseModel>>?


    @POST("/v3/identityCard/clear")
    @FormUrlEncoded
    fun clearIccardOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/fingerprint/clear")
    @FormUrlEncoded
    fun clearFingerprintOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/gateway/list")
    @FormUrlEncoded
    fun getGatewayAccountListOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("pageNo") pageNo: Int,
        @Field("pageSize") pageSize: Int,
        @Field("date") date: Long
    ):Deferred<Response<GatewayAccList>>?


    @POST("/v3/lock/detail")
    @FormUrlEncoded
    fun getKeyDetailsOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long
    ): Deferred<Response<LockDetails>>?

    @POST("/v3/lock/delete")
    @FormUrlEncoded
    fun deleteLockOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?


    @POST("/v3/identityCard/delete")
    @FormUrlEncoded
    fun deleteIcCardOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("cardId") cardId: Int,
        @Field("deleteType") deleteType: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/fingerprint/delete")
    @FormUrlEncoded
    fun deleteFingerprintOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("fingerprintId") fingerprintId: Int,
        @Field("deleteType") deleteType: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/key/delete")
    @FormUrlEncoded
    fun deleteeAccessKeyOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("keyId") keyId: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/keyboardPwd/delete")
    @FormUrlEncoded
    fun deleteePasscodeOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("keyboardPwdId") keyboardPwdId: Int,
        @Field("deleteType") deleteType: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/key/authorize")
    @FormUrlEncoded
    fun authorizeEAccessOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("keyId") keyId: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/key/list")
    @FormUrlEncoded
    fun getEkeyListOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("pageNo") pageNo: Int,
        @Field("pageSize") pageSize: Int,
        @Field("date") date: Long
    ):Deferred<Response<LockKeyListMode>>?

    @POST("/v3/keyboardPwd/add")
    @FormUrlEncoded
    fun addCustomPasscodeOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("keyboardPwd") keyboardPwd: String,
        @Field("keyboardPwdName") keyboardPwdName: String?,
        @Field("startDate") startDate: Long,
        @Field("endDate") endDate: Long,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/lock/updateElectricQuantity")
    @FormUrlEncoded
    fun uploadBatteryStatusOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("lockId") lockId: Int,
        @Field("electricQuantity") electricQuantity: Int,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?


    @POST("/v3/gateway/uploadDetail")
    @FormUrlEncoded
    fun uploadGatewayDetailsOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("gatewayId") gatewayId: Int,
        @Field("modelNum") modelNum: String,
        @Field("hardwareRevision") hardwareRevision: String,
        @Field("firmwareRevision") firmwareRevision: String,
        @Field("networkName") networkName: String,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/gateway/isInitSuccess")
    @FormUrlEncoded
    fun initGatewayOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") clientSecret: String?,
        @Field("gatewayNetMac") gatewayNetMac: String?,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/lock/lock")
    @FormUrlEncoded
    fun lockViaGatewayOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?


    @POST("/v3/lock/unlock")
    @FormUrlEncoded
    fun unLockViaGatewayOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/gateway/delete")
    @FormUrlEncoded
    fun deleteGatewayOpenTT(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("gatewayId") gatewayId: Int?,
        @Field("date") date: Long
    ):Deferred<Response<ApiResponseModel>>?

    @POST("/v3/lock/queryOpenState")
    @FormUrlEncoded
    fun getLockStatus(
        @Query("clientId") clientId: String?,
        @Field("accessToken") accessToken: String?,
        @Field("lockId") lockId: Int?,
        @Field("date") date: Long
    ):Deferred<Response<LockStatus>>?
    //-------------------------------- Http Call ------------------------------------

    companion object{
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor
        ) : RudoApi{
            val gson = GsonBuilder()
                .setLenient()
                .create()
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            //logging.setLevel(HttpLoggingInterceptor.Level.HEADERS)
            val interceptor =
                Interceptor { chain: Interceptor.Chain ->
                    val original = chain.request()
                    // Request customization: add request headers
                    val requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/x-www-form-urlencoded")
                        .addHeader("Accept", "application/json")
                    val request = requestBuilder.build()
                    chain.proceed(request)
                }

            val okkHttpclient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .addInterceptor(interceptor)
                .addInterceptor(logging)
                .build()

            return Retrofit.Builder()
                .client(okkHttpclient)
                .baseUrl(BuildConfig.BASE_HOST)
               .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(RudoApi::class.java)
        }
    }

}