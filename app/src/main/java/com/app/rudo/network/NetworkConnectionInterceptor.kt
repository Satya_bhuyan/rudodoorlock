package com.app.rudo.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.app.rudo.utils.NoInternetException
import okhttp3.Interceptor
import okhttp3.Response


// Created by Satyabrata Bhuyan  on 29-06-2020.
// Company Yutu Electronics
// E_Mail  s.bhuyan0037@gmail.com

class NetworkConnectionInterceptor(context: Context) : Interceptor {
    private val applicationContext = context.applicationContext
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!isInternetAvalable()) {
             throw NoInternetException("No Internet Connection")
        }
        return chain.proceed(chain.request())
    }

    private fun isInternetAvalable(): Boolean {
        var result = false
       val connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        connectivityManager?.let {
            it.getNetworkCapabilities(connectivityManager.activeNetwork)?.apply {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    else -> false
                }
            }
        }
        return result
    }
}