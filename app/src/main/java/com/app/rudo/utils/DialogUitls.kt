package com.app.rudo.utils

import android.app.Dialog
import android.content.Context
import android.text.InputType
import android.text.TextUtils
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import java.lang.ref.WeakReference

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class DialogUitls() {

    companion object {
        fun showDialog(
            context: Context,
            titel: String,
            message: String,
            yesButton:Boolean?,
            noButton:Boolean?,
            canelable:Boolean,
            listner: OnClickDialogItemImpl
        ) {
            var dialogAleart: Dialog?
            val builder = AlertDialog.Builder(context)
            dialogAleart = builder.create()
            builder.setTitle(titel)
            builder.setMessage(message)
            builder.setCancelable(canelable)

            if (yesButton == true) {
                builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                    listner.onClickYes(dialogAleart)
                }
            }
            if(noButton == true) {
                builder.setNegativeButton(android.R.string.no) { dialog, which ->
                    listner.onClickNo(dialogAleart)
                }
            }

            /*builder.setNeutralButton("Maybe") { dialog, which ->

            }*/
            builder.show()
        }

         fun showEditTextAlert(context: Context, listner: OnClickDialogItemImpl) {
             val weakReference = WeakReference(context)
            val editText = EditText(weakReference.get())
             editText.hint = "Enter Password"
             editText.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT)
             editText.layoutParams = layoutParams
             var dialogAleart: Dialog?
             val alertDialog = AlertDialog.Builder(weakReference.get()!!)
             dialogAleart = alertDialog.create()
             alertDialog.setTitle("Please Enter Password")
             alertDialog.setView(editText)
             alertDialog.setCancelable(false)
             alertDialog.setPositiveButton("Delete") { dialog, which ->
                    if(TextUtils.isEmpty(editText.text.toString())){
                        Toast.makeText(weakReference.get(), "Empty Password", Toast.LENGTH_LONG).show()
                    }else{
                        listner.onClickDialogDelete(editText.text.toString())
                    }
                }
                .setNegativeButton("Cancel") { dialog, which ->
                    dialog.dismiss()
                }
               alertDialog .show()
        }
        fun showRemoteUnlockDialog(context: Context, listner: OnClickDialogItemImpl) {
            val dialogAleart: Dialog?
            val builder = AlertDialog.Builder(context)
            dialogAleart = builder.create()
            builder.setMessage("Unlock Remotely?")
            builder.setCancelable(false)

                builder.setPositiveButton("unlock remotely") { dialog, which ->
                    listner.onRemoteUnlock(dialogAleart)
                }


                builder.setNegativeButton("Cancel") { dialog, which ->
                    listner.onClickNo(dialogAleart)
                }


            /*builder.setNeutralButton("Maybe") { dialog, which ->

            }*/
            builder.show()
        }
    }

    interface OnClickDialogItemImpl {
        fun onClickYes(dialog: Dialog)
        fun onClickNo(dialog: Dialog)
        fun onClickDialogDelete(password:String)
        fun onClickCancel(dialog: Dialog)
        fun onRemoteUnlock(dialog: Dialog)
    }
}