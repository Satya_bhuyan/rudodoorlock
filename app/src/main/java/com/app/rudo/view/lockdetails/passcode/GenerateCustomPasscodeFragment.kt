package com.app.rudo.view.lockdetails.passcode

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentCustomPasscodeBindingImpl
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.CreateCustomPasscodeCallback
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_custom_passcode.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


/*
// Created by Satyabrata Bhuyan on 18-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GenerateCustomPasscodeFragment(private var lock: Lock) : Fragment(),
    DialogUitls.OnClickDialogItemImpl, GeneratePasscodeImpl,
    KodeinAware, DateUtils.Companion.DateTimePickerImpl {
    override val kodein: Kodein by kodein()
    private val factory: GeneratePassCodeViewmodelFactory by instance<GeneratePassCodeViewmodelFactory>()
    private var viewModel: GeneratePasscodeViewModel? = null
    private var dialog: AlertDialog? = null
    private var startTimeMill = 0L
    private var endTimeMill = 0L
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentCustomPasscodeBindingImpl =
            DataBindingUtil.inflate(inflater, R.layout.fragment_custom_passcode, container, false)
        ensureBluetoothIsEnabled()
        viewModel = ViewModelProvider(this, factory).get(GeneratePasscodeViewModel::class.java)
        viewModel?.registerListner(this)
        viewModel?.setkeyboardPwdType(Constants.Custom)
        viewModel?.lock = lock
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val startTime = DateUtils.getCurrentDateTime("startTime")
        //val endTime = DateUtils.getCurrentDateTime("startTime")
        txtStartTime.text = DateUtils.getFormatedDateAndTime(startTime)
        txtEndtime.text = DateUtils.getFormatedDateAndTime(startTime)
        viewModel?.startTime = startTime
        viewModel?.endTime = startTime
        txtStartTime.setOnClickListener {
            DateUtils.getDatePicker(requireContext(), Constants.START_TIME, this)
        }
        txtEndtime.setOnClickListener {
            DateUtils.getDatePicker(requireContext(), Constants.END_TIME, this)
        }
        switchPermanent.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                viewModel?.startTime = 0L
                llStartTime.visibility = View.GONE
                llEndTime.visibility = View.GONE
            } else {
                viewModel?.endTime = 0L
                llStartTime.visibility = View.VISIBLE
                llEndTime.visibility = View.VISIBLE
            }
        }

        /* if (it.is) {
             llStartTime.visibility = View.GONE
             llEndTime.visibility = View.GONE
         } else {
             llStartTime.visibility = View.VISIBLE
             llEndTime.visibility = View.VISIBLE
         }*/


        buttonSetPasscode.setOnClickListener {
            setCustomPasscode()
        }
    }

    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progress_bar?.show()
    }

    override fun onSuccess(message: String) {
        edtName.text = null
        edtPasscode.text = null
        progress_bar?.hide()
        dialog = showAlertDialog {
            eText.text = message
            btnClickListener {
                dialog?.cancel()
            }
            imgShare.visibility = View.VISIBLE
            shareClickListener {
                // dialog?.dismiss()
                val sendIntent = Intent()
                sendIntent.setAction(Intent.ACTION_SEND)
                // sendIntent.setPackage("com.whatsapp")
                sendIntent.putExtra(Intent.EXTRA_TEXT, message)
                sendIntent.setType("text/plain")
                startActivity(sendIntent)
            }
        }
        dialog?.setCancelable(false)

        dialog?.show()
        // DialogUitls.showDialog(requireContext(),"Alert",message,true,false,this@GeneratePasscodeTimedFragment)

    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        view?.rootView?.snackbar(message)
    }

    override fun onTimeError() {
        requireContext().toast(getString(R.string.time_error))
    }


    override fun onClickYes(dialog: Dialog) {

    }

    override fun onClickNo(dialog: Dialog) {

    }

    override fun onClickDialogDelete(password: String) {

    }


    override fun onClickCancel(dialog: Dialog) {

    }

    override fun onRemoteUnlock(dialog: Dialog) {

    }

    override fun setDateTime(millsec: Long, identifyer: String) {
        val temp = DateUtils.getFormatedDateAndTime(millsec)
        if (Constants.START_TIME.equals(identifyer, ignoreCase = true)) {
            txtStartTime.text = temp
            startTimeMill = millsec
            viewModel?.startTime = millsec
        } else if (Constants.END_TIME.equals(identifyer, ignoreCase = true)) {
            viewModel?.endTime = millsec
            endTimeMill = millsec
            txtEndtime.text = temp
        }
    }

    private fun ensureBluetoothIsEnabled() {
        if (!TTLockClient.getDefault().isBLEEnabled(context)) {
            TTLockClient.getDefault().requestBleEnable(activity)
        }
        // setLockTime()

    }

    private fun setCustomPasscode() {

        if (TextUtils.isEmpty(edtPasscode.text.toString())) {
            requireContext().toast("Please Enter Passcode")
            return
        }
        if (!switchPermanent.isChecked) {
            if (viewModel?.startTime!! >= viewModel?.endTime!!) {
                //view?.rootView?.snackbar("Start Time and End Time can not be same")
                requireContext().toast(requireContext().getString(R.string.time_error))
                return
            }
        }
        var name = ""
        if (!TextUtils.isEmpty(edtName.text.toString())) {
            name = edtName.text.toString()
        }
        progress_bar?.show()
        lock?.let {
            TTLockClient.getDefault().createCustomPasscode(
                edtPasscode.text.toString(),
                startTimeMill,
                endTimeMill,
                lock?.lockData,
                lock?.lockMac,
                object : CreateCustomPasscodeCallback {
                    override fun onCreateCustomPasscodeSuccess(p0: String?) {
                        viewModel?.addCustomPasscode(edtPasscode.text.toString(), name)
                    }

                    override fun onFail(p0: LockError?) {
                        progress_bar?.hide()
                        if(p0?.name == "LOCK_CONNECT_FAIL"){
                            requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                        }
                    }
                }
            )
        }
    }
}