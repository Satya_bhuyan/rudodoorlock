package com.app.rudo.view.settings

import android.Manifest
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentLockSettingsBinding
import com.app.rudo.model.lockdetails.LockDetails
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.ResetLockCallback
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_lock_settings.*
import kotlinx.android.synthetic.main.fragment_settings.progress_bar
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import timber.log.Timber

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class LockSettingFragment : Fragment(), KodeinAware, SettingsImpl,
    DialogUitls.OnClickDialogItemImpl {
    override val kodein: Kodein by kodein()
    val factory: SettingViewmodelFactory by instance<SettingViewmodelFactory>()
    var viewModel: SettingViewModel? = null
    var lock: Lock? = null
    var lockData: LockDetails? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentLockSettingsBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_lock_settings, container, false)
        viewModel = ViewModelProvider(this, factory).get(SettingViewModel::class.java)
        binding.locksettingvm = viewModel
        viewModel?.registerListner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            lock = (Lock::class.java).cast(arguments?.getSerializable("lockData"))
            viewModel?.lock = lock

            if(Constants.ADMIN_KEY == lock?.userType) {
                viewModel?.getLockDeatils(lock?.lockId!!)
            }else{
                rlUnlockRemotly.visibility = View.GONE
                rlLockSound.visibility = View.GONE
                llBasics.visibility = View.GONE
                flDelete.visibility = View.GONE
            }
        }
        /**
         * this should be called first,to make sure Bluetooth configuration is ready.
         */
        if (AppUtil.isAndroid12OrOver()) {
            AppUtil.checkPermission(requireActivity(), Manifest.permission.BLUETOOTH_CONNECT)
        }
        TTLockClient.getDefault().prepareBTService(requireContext())
    }

    override fun onStarted() {
        progress_bar?.show()
    }

    override fun onLogout() {
        progress_bar?.hide()

        view?.findNavController()?.navigate(R.id.action_locksettings_to_home)
        /* Intent(requireContext(), LoginActivity::class.java).also {
             it.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
             it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
             requireContext().startActivity(it)
             requireActivity().finish()
         }*/
    }

    override fun onDeleteOntion() {


    }

    override fun onDeleteOntion(lockDetails: LockDetails) {
        lockData = lockDetails
        DialogUitls.showDialog(
            requireContext(),
            "Delete Lock",
            "Do you want to delete Lock?",
            true,
            true,
            false,
            this

        )
    }

    override fun onError(message: String) {
        progress_bar?.hide()
        view?.rootView?.snackbar(message)
    }

    override fun onSuccessLogin() {
        resetLock()
    }

    override fun onClickYes(dialog: Dialog) {
        DialogUitls.showEditTextAlert(requireContext(),this@LockSettingFragment)

       // viewModel?.deleteLock(lock?.lockId!!)
    }

    override fun onClickNo(dialog: Dialog) {
        dialog?.cancel()
    }

    override fun onClickDialogDelete(password: String) {
        viewModel?.login(password)
    }


    override fun onClickCancel(dialog: Dialog) {

    }

    override fun onRemoteUnlock(dialog: Dialog) {

    }

    private fun resetLock() {

        if (!TTLockClient.getDefault().isBLEEnabled(requireContext())) {
            TTLockClient.getDefault().requestBleEnable(requireActivity())
        }
      //  resetKey()
        TTLockClient.getDefault().resetLock(
            lock?.lockData,
            lock?.lockMac,
            object : ResetLockCallback {
                override fun onResetLockSuccess() {
                    viewModel?.deleteLock(lock?.lockId!!)
                    // uploadResetLock2Server()
                }

                override fun onFail(error: LockError) {
                    progress_bar?.hide()
                    Timber.e("Error delete lock", error.toString())
                    onError(error.toString())
                    // makeErrorToast(error)
                }
            })
    }

   /* private fun resetKey() {
        TTLockClient.getDefault().resetEkey(
            lock?.lockData,
            lock?.lockMac,
            object : ResetKeyCallback {
                override fun onFail(error: LockError) {
                    Timber.e("Error delete lock", error.errorMsg)
                    // makeErrorToast(error)
                }

                override fun onResetKeySuccess(lockData: String?) {

                }

            })
    }*/

}