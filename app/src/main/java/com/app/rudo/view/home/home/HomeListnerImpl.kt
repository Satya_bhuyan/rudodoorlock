package com.app.rudo.view.home.home

/*
// Created by Satyabrata Bhuyan on 04-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface HomeListnerImpl {
    fun onStarted()
    fun onSuccess()
    fun onFailure(message:String)
}