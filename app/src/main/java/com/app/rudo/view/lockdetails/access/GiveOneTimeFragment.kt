package com.app.rudo.view.lockdetails.access

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.databinding.FragmentGiveOntimeBinding
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import kotlinx.android.synthetic.main.fragment_give_ontime.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GiveOneTimeFragment(private var lock: Lock) : Fragment(), GiveAccessImpl, KodeinAware,
    DialogUitls.OnClickDialogItemImpl {
    override val kodein: Kodein by kodein()
    private val factory: GiveAccessViewModelFactory by instance<GiveAccessViewModelFactory>()
    private var viewModel: GiveAccessViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentGiveOntimeBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_give_ontime,
            container,
            false
        )
        viewModel = ViewModelProvider(this, factory).get(GiveAccessViewModel::class.java)
        viewModel?.registerListner(this)
        binding.viewmodel = viewModel
        viewModel?.lockId = lock.lockId
        viewModel?.lock = lock
        viewModel?.remarks = "OneTime"
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val startTime =DateUtils.getCurrentDateTime("startTime")
        val endTime =DateUtils.getCurrentDateTime("endTime")
       // stateTimeTimed.text = DateUtils.getFormatedDateAndTime(startTime)
       // endDateTimed.text = DateUtils.getFormatedDateAndTime(endTime)
        viewModel?.startTime = startTime
        viewModel?.endTime = endTime
        swRemoteUnlock?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked){
                viewModel?.remoteAllow = 1
            }else{
                viewModel?.remoteAllow = 2
            }
        }
    }
    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progressBarGiveOnetime.show()
    }

    override fun onSuccess() {
        edtRecipients.setText("")
        edtEkeyName.setText("")
        progressBarGiveOnetime.hide()
        DialogUitls.showDialog(
            requireContext(),
            "",
            "eAccess shared successfully",
            true,
            false,
            true,
            this
        )
    }

    override fun onFailure(message: String) {
        progressBarGiveOnetime.hide()
        view?.rootView?.snackbar(message)
    }

    override fun onTimeError() {
        requireContext().toast(getString(R.string.time_error))
    }

    override fun onClickYes(dialog: Dialog) {
        dialog?.dismiss()
        view?.findNavController()?.popBackStack()
    }

    override fun onClickNo(dialog: Dialog) {

    }

    override fun onClickDialogDelete(password: String) {

    }


    override fun onClickCancel(dialog: Dialog) {

    }

    override fun onRemoteUnlock(dialog: Dialog) {

    }
}