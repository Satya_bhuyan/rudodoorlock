package com.app.rudo.view.addlock

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.databinding.FragmentSetDevicenameBinding
import com.app.rudo.utils.hide
import com.app.rudo.utils.hideKeyboard
import com.app.rudo.utils.show
import com.app.rudo.utils.snackbar
import kotlinx.android.synthetic.main.fragment_set_devicename.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import timber.log.Timber

/*
// Created by Satyabrata Bhuyan on 17-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class DeviceNameFragment : Fragment(), AddLockImpl, KodeinAware {

    override val kodein: Kodein by kodein()
    private val factory: AddLockViewmodelFactory by instance<AddLockViewmodelFactory>()
    private var viewModel: AddLockViewModel? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentSetDevicenameBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_set_devicename,
            container,
            false
        )
        viewModel = ViewModelProvider(this, factory).get(AddLockViewModel::class.java)
        binding.vm = viewModel
        viewModel?.registerListner(this)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            val lockData = arguments?.getString("lockData")
            lockData?.let {
                Timber.i("lockData %s",lockData)
                viewModel?.lockData = lockData
            }
        }
    }

    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progress_bar.show()
    }

    override fun onSuccess() {
        progress_bar.hide()
        view?.findNavController()?.navigate(R.id.homeFragment)
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        view?.rootView?.snackbar(message)
    }
}