package com.app.rudo.view.gateway

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.databinding.GatewayAccItemBinding
import com.app.rudo.model.gateway.AccList
import com.app.rudo.model.gateway.GatewayItemDetails

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GatewayAccAdapter (
    val listData : List<AccList>,
    val impl:ItemClick
): RecyclerView.Adapter<GatewayAccAdapter.GatewayHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=GatewayHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.gateway_acc_item,
            parent,
            false
        )
    )

    override fun getItemCount() = listData.size

    override fun onBindViewHolder(holder: GatewayHolder, position: Int) {
        holder.gatewayItem.gatewayitem = listData[position]
        holder.gatewayItem.root.setOnClickListener {

            impl.onClickItem(GatewayItemDetails(listData[position].gatewayId,
            listData[position].gatewayMac,
            listData[position].isOnline,
            listData[position].lockNum,
            listData[position].gatewayName,
            listData[position].networkName,
            listData[position].gatewayVersion,
            listData[position].status))
        }

    }

    inner class GatewayHolder(
        val gatewayItem: GatewayAccItemBinding
    ): RecyclerView.ViewHolder(gatewayItem.root)

    interface ItemClick{
        fun onClickItem(data:GatewayItemDetails)
    }
}