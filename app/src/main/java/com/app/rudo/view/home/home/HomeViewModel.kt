package com.app.rudo.view.home.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.rudo.model.eaccess.EAccessKeyList
import com.app.rudo.model.locklist.LockDetailsData
import com.app.rudo.repository.HomeRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import com.app.rudo.utils.ServerException
import kotlinx.coroutines.launch

class HomeViewModel(
    private val repository: HomeRepository,
    private val pref: AppPrefrences
) : ViewModel() {

   // private val _lockList = MutableLiveData<List<Lock>>()
    private val _lockKeyList = MutableLiveData<List<EAccessKeyList>>()
   // val lockList: LiveData<List<Lock>> = _lockList
    var lockKeyList: LiveData<List<EAccessKeyList>>? = _lockKeyList
    var _tempLockList: MutableList<LockDetailsData>? = null

    // get() = _lockList

    private var homeListnerImpl: HomeListnerImpl? = null
    fun registerListner(homeListnerImpl: HomeListnerImpl) {
        this.homeListnerImpl = homeListnerImpl
    }

    fun getLockList() {
        if (_tempLockList.isNullOrEmpty()){
            _tempLockList?.clear()
        }
        homeListnerImpl?.onStarted()
        try {
            viewModelScope.launch {
                val result = repository.getLockList()
                result.let {
                    if(null != result.list ){
                        _tempLockList = result.list!!
                        getEKeyList()
                    }else{
                        homeListnerImpl?.onFailure(result.errorMessage!!)
                    }
                    //_lockList.value = it.list
                }
            }
        } catch (e: ApiException) {
            homeListnerImpl?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            homeListnerImpl?.onFailure(e.message!!)
        }catch (e:ServerException){
            homeListnerImpl?.onFailure(e.message!!)
        }

    }

    /*  private fun getLockListFromYutu() {
          homeListnerImpl?.onStarted()
          viewModelScope.launch {
              try {
                  val result = repository.getLockList()
                  result.let {
                      if (it.list != null) {
                          if (it.list.size > 0) {
                              tempLockList = it.list
                              _lockList.value = it.list
                              homeListnerImpl?.onSuccess()
                          } else {
                              homeListnerImpl?.onFailure("No Lock is added")
                          }
                      } else {
                          it.errmsg?.let {
                              homeListnerImpl?.onFailure(it)
                          }
                      }
                  }

              } catch (e: ApiException) {
                  homeListnerImpl?.onFailure(e.message!!)
              } catch (e: NoInternetException) {
                  homeListnerImpl?.onFailure(e.message!!)
              }
          }
      }

      private fun getLockListFromOpenTTApi() {
          viewModelScope.launch {
              try {
                  val result = repository.getLockListFromOpenTTApi()
                  result.let {
                      tempLockList = result.list
                      _lockList.value = it.list
                  }
              } catch (e: ApiException) {
                  homeListnerImpl?.onFailure(e.message!!)
              } catch (e: NoInternetException) {
                  _lockList.value = pref.getLockData().list
                  // homeListnerImpl?.onFailure(e.message!!)
              }
          }
      }*/

    fun getEKeyList() {
        try {
            viewModelScope.launch {
                    val result = repository.getEkeyList()
                result.let {
                    if(result.list.isNullOrEmpty()){
                        result.list = emptyList()
                    }

                    for (i in 0..result.list?.size!!-1){
                        for (j in 0.._tempLockList!!.size-1){
                            if (result.list!![i].lockAlias == _tempLockList!![j].lockAlias){
                                result.list!![i].hasGateway = _tempLockList!![j].hasGateway
                            }
                        }
                    }
                    //result.list?.toMutableList()?.addAll(listOf(_tempLockList!! as EAccessKeyList))

                    // if (result.list?.size!! >0) {
                    _lockKeyList.value = result.list
                    /*} else {
                                        homeListnerImpl?.onFailure("No Lock is Added")
                                    }*/

                }
            }
        } catch (e: ApiException) {
            homeListnerImpl?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
           // _lockList.value = pref.getLockData().list
             homeListnerImpl?.onFailure(e.message!!)
        }catch (e:ServerException){
            homeListnerImpl?.onFailure(e.message!!)
        }
    }

}