package com.app.rudo.view.gateway

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.app.rudo.R
import kotlinx.android.synthetic.main.fragment_gateway_device.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/
class GatewayDeviceFragment : Fragment(), KodeinAware {
    override val kodein: Kodein by kodein()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gateway_device,container,false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        optionClick()
    }
     fun optionClick(){
         llItem.setOnClickListener {
             view?.findNavController()?.navigate(R.id.action_gateway_device_to_info)
         }
     }

}