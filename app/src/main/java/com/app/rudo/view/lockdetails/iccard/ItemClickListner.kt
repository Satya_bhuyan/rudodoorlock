package com.app.rudo.view.lockdetails.iccard

import com.app.rudo.model.fingerprint.FingerprintData
import com.app.rudo.model.iccard.IccardData

/*
// Created by Satyabrata Bhuyan on 10-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface ItemClickListner {
    fun deleteOnClick(iccardData: IccardData)
    fun deleteFingerprintOnClick(fingerprintData: FingerprintData)
}