package com.app.rudo.view.lockdetails.passcodehistory

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentPasscodeHistoryItemBinding
import com.app.rudo.model.PassCodeListModel

/*
// Created by Satyabrata Bhuyan on 10-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class PasscodeHistoryAdapter(
    val listData : MutableList<PassCodeListModel>,
    val listner : ItemPasscodeClicked
): RecyclerView.Adapter<PasscodeHistoryAdapter.PassCodeHolder>(){
    private var itemsPendingRemoval: MutableList<PassCodeListModel>? = mutableListOf()
    private var PENDING_REMOVAL_TIMEOUT: Long = 3000
    var handler: Handler? = Handler()
    var pendingRunnables: HashMap<PassCodeListModel, Runnable>? = HashMap()
    init {
        itemsPendingRemoval = mutableListOf<PassCodeListModel>()
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=PassCodeHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.fragment_passcode_history_item,
            parent,
            false
        )
    )

    override fun getItemCount() = listData.size

    override fun onBindViewHolder(holder: PassCodeHolder, position: Int) {
        holder.fragmentPasscodeHistoryItemBinding.passhistory = listData[position]
        listData[position].keyboardPwdType.let {
            var periodType:String? = ""
            when (listData[position].keyboardPwdType) {

                Constants.Permanent -> {
                    periodType = "Permanent"
                    holder.fragmentPasscodeHistoryItemBinding.type.text = "(Permanent)"
                }
                Constants.Period -> {
                    periodType = "Timed"
                    holder.fragmentPasscodeHistoryItemBinding.type.text = "(Timed)"
                }
                Constants.Onetime -> {
                    periodType = "One-Time"
                    holder.fragmentPasscodeHistoryItemBinding.type.text = "(One-Time)"
                }
                else ->{
                    periodType = "Custom"
                    holder.fragmentPasscodeHistoryItemBinding.type.text = "(Custom)"
                }

            }
            listData[position].validityPeriod = periodType
        }

        if (itemsPendingRemoval!!.contains(listData[position])) {
            //show swipe layout
            holder.fragmentPasscodeHistoryItemBinding.swipeLayout.visibility = View.VISIBLE
            holder.fragmentPasscodeHistoryItemBinding.itemRoot.visibility = View.GONE

            holder.fragmentPasscodeHistoryItemBinding.txtUndo.setOnClickListener {
                undoOpt(listData[position])
            }
            holder.fragmentPasscodeHistoryItemBinding.txtDelete.setOnClickListener {
                listner?.deletePasscodeItem(listData[position])
                // remove(position)
            }

        } else {
            //show regular layout
            holder.fragmentPasscodeHistoryItemBinding.swipeLayout.visibility = View.GONE
            holder.fragmentPasscodeHistoryItemBinding.itemRoot.visibility = View.VISIBLE

            /* itemView.txt.text = model.name
             itemView.sub_txt.text = model.version
             val id = context.resources.getIdentifier(model.name.toLowerCase(), "drawable", context.packageName)
             itemView.img.setBackgroundResource(id)*/
        }
        holder.fragmentPasscodeHistoryItemBinding.root.setOnClickListener{
            listner?.itemSelected(listData[position])
        }
    }

    inner class PassCodeHolder(
        val fragmentPasscodeHistoryItemBinding: FragmentPasscodeHistoryItemBinding
    ): RecyclerView.ViewHolder(fragmentPasscodeHistoryItemBinding.root)
    fun undoOpt(model: PassCodeListModel) {
        val pendingRemovalRunnable: Runnable? = pendingRunnables?.get(model)
        pendingRunnables?.remove(model)
        if (pendingRemovalRunnable != null)
            handler?.removeCallbacks(pendingRemovalRunnable)
        itemsPendingRemoval?.remove(model)
        // this will rebind the row in "normal" state
        notifyItemChanged(listData.indexOf(model))
    }

    fun pendingRemoval(position: Int) {

        val data = listData[position]
        if (!itemsPendingRemoval!!.contains(data)) {
            itemsPendingRemoval?.add(data)
            // this will redraw row in "undo" state
            notifyItemChanged(position)
            // let's create, store and post a runnable to remove the data
            /* val pendingRemovalRunnable = Runnable {
                 remove(listData.indexOf(data))
             }

             handler?.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT)
             // pendingRunnables!![data] = pendingRemovalRunnable
             pendingRunnables?.put(data, pendingRemovalRunnable)*/
        }
    }

    fun remove(position: Int) {
        val data = listData.get(position)
        if (itemsPendingRemoval!!.contains(data)) {
            itemsPendingRemoval?.remove(data)
        }
        if (listData.contains(data)) {
            //dataList.remove(position)
            listData.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun isPendingRemoval(position: Int): Boolean {
        val data = listData[position]
        return itemsPendingRemoval!!.contains(data)
    }

    interface ItemPasscodeClicked{
        fun deletePasscodeItem(passcodeData: PassCodeListModel)
        fun itemSelected(passcodeData: PassCodeListModel)
    }
}