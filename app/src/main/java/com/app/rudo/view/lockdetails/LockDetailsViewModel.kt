package com.app.rudo.view.lockdetails

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.app.rudo.BuildConfig
import com.app.rudo.R
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.locklist.Lock
import com.app.rudo.repository.LockDetailRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.NoInternetException
import kotlinx.coroutines.launch
import timber.log.Timber
import java.net.URLDecoder

class LockDetailsViewModel(
    private val repository: LockDetailRepository
) : ViewModel() {
    private var lockDeatilsImpl: LockDeatilsImpl? = null
    private var selectedLockId: Int? = null
    var passCodeListModel: PassCodeListModel? = null
    var eAccessKey: EAccessKey? = null

    // var keyData:KeyData? = null
    var lock: Lock? = null
    fun registerListner(lockDeatilsImpl: LockDeatilsImpl) {
        this.lockDeatilsImpl = lockDeatilsImpl
    }

    fun setSelectedLockId(lockId: Int) {
        selectedLockId = lockId
    }

    fun onClickGiveAccess(view: View) {
        val args = Bundle()
        args.putSerializable("lockData", lock!!)
        view.findNavController()
            .navigate(R.id.action_lockDetailsFragment_to_giveAccessFragment, args)
    }

    fun onClickGeneratePassCode(view: View) {
        //val args = bundleOf("lock" to selectedLockId)
        val args = Bundle()
        args.putSerializable("lockData", lock!!)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_passcodeFragment, args)
    }

    fun onClickAccessHistory(view: View) {
        val args = bundleOf("lock" to selectedLockId)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_accessHistory, args)
    }

    fun onClickPasscodeHistory(view: View) {
        val args = Bundle()
        args.putSerializable("lockData", lock!!)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_passcodeHistory, args)
    }

    fun onClickIcCard(view: View) {
        //val args = bundleOf("lock" to selectedLockId)
        val args = Bundle()
        args.putSerializable("lockData", lock!!)
        args.putString("type", "IcCard")
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_iccardFragment, args)

    }

    fun onClickFingerPrint(view: View) {
        val args = Bundle()
        args.putSerializable("lockData", lock!!)
        args.putString("type", "Fingerprint")
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_iccardFragment, args)
    }

    fun onClickRecords(view: View) {
        val args = bundleOf("lock" to selectedLockId)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_records, args)
    }

    fun onClickSettings(view: View) {

        //val args = bundleOf("lock" to selectedLockId)
        val args = Bundle()
        args.putSerializable("lockData", lock)
        view.findNavController().navigate(R.id.action_lockDetailsFragment_to_settings, args)
    }

    /*fun onClickLock(view: View) {
       // lockDeatilsImpl?.onClickLockImage()
    }*/


    fun getLockList(lockId: Int) {
        try {
            lockDeatilsImpl?.onStarted()
            viewModelScope.launch {
                val result = repository.getKeyList(lockId)
                result.let {
                    // lock = it
                    lockDeatilsImpl?.onSuccesLockDetails(it)
                }
            }
        } catch (e: ApiException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: Exception) {

        }
    }


    fun getLockKey(lockId: Int) {
        lockDeatilsImpl?.onStarted()
        try {
            viewModelScope.launch {
                val result = repository.getLockKey(lockId)
                result.let {
                    if (it.list != null) {
                        lockDeatilsImpl?.onSuccessLockKey(it.list)
                    } else {
                        it.errmsg?.let {
                            lockDeatilsImpl?.onFailure(it!!)
                        }
                    }
                }

            }
        } catch (e: ApiException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: Exception) {

        }
    }


    fun getPassCodes(lockId: Int) {
        try {
            viewModelScope.launch {
                val result = repository.getPassCodes(lockId)
                result?.let {
                    if (it.list != null) {
                        lockDeatilsImpl?.onSuccessPassCodes(it.list!!)
                    } else {
                        it.errmsg.let {
                            lockDeatilsImpl?.onFailure(it!!)
                        }
                    }
                }
            }
        } catch (e: ApiException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: Exception) {

        }
    }

    fun uploadRecords(logs: String) {
        // lockDeatilsImpl?.onStarted()
        val decoded: String = URLDecoder.decode(logs, "UTF-8")

        try {
            viewModelScope.launch {
                val result = repository.uploadRecodes(selectedLockId!!, decoded)
                result.let {
                    if (it.errmsg.isNullOrEmpty()) {
                        lockDeatilsImpl?.onSuccess()
                    } else {
                        lockDeatilsImpl?.onFailure(it.errmsg)
                    }

                }
            }
        } catch (e: ApiException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: Exception) {

        }
    }

    fun getUnlockRecords() {
        lockDeatilsImpl?.onStarted()
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            getUnlockRecodesOpenTTApi()
        } else {
            getUnlockRecodes()
        }
    }

    private fun getUnlockRecodesOpenTTApi() {
        viewModelScope.launch {
            try {
                val result = repository.getUnloackRecodsOpenTTApi(selectedLockId!!)
                result?.list?.let {
                    lockDeatilsImpl?.onSuccessUnlockRecords(it)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun getUnlockRecodes() {
        viewModelScope.launch {
            try {
                val result = repository.getUnloackRecods(selectedLockId!!)
                result?.list?.let {
                    lockDeatilsImpl?.onSuccessUnlockRecords(it)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    fun deleteeAccessKey(eAccessKey: EAccessKey) {
        viewModelScope.launch {
            try {
                val result = repository.deleteeAccessKey(eAccessKey.keyId)
                result.let {
                    getLockKey(eAccessKey.lockId)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    fun deleteeAccessKeyOneTime(keyId: Int) {
        viewModelScope.launch {
            try {
                val result = repository.deleteeAccessKey(keyId)
                result.let {
                    // getLockKey(eAccessKey.lockId)
                    if (result.errcode == 0) {
                        lockDeatilsImpl?.onEAccessKeyDeleteSuccess()
                    }
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    fun deletePasscode(passcodeData: PassCodeListModel, deleteType: Int) {
        viewModelScope.launch {
            try {
                val result =
                    repository.deletePasscode(passcodeData.lockId!!, passcodeData.keyboardPwdId!!, deleteType)
                result?.let {
                    getPassCodes(passcodeData.lockId)
                }

            } catch (e: ApiException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                lockDeatilsImpl?.onFailure(e.message!!)
            }
        }
    }

    fun deletePasscodeFromDetails(view: View) {
        lockDeatilsImpl?.onDeletePasscodeFromDetails(passCodeListModel!!)
        // deletePasscode(passCodeListModel!!)
    }

    fun deleteEAccessFromDetails(view: View) {
        deleteeAccessKey(eAccessKey!!)
    }

    fun uploadBatteryStatus(lockId: Int, batteryStatus: Int) {
        try {
            viewModelScope.launch {
                val response = repository.uploadBatteryStatus(
                    lockId,
                    batteryStatus
                )
            }
        } catch (e: ApiException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            lockDeatilsImpl?.onFailure(e.message!!)
        }
    }

    fun lockViagateway(lockId: Int) {
        lockDeatilsImpl?.onStarted()
        try {
            viewModelScope.launch {
                val response = repository.lockViaGateway(lockId)
                response?.let {
                    if (response.errcode == 0) {
                        // lockDeatilsImpl?.onSuccess()
                       // getLockStatus()
                        lockDeatilsImpl?.onRemoteLockUnLockSuccess("Locked")
                    } else {
                        lockDeatilsImpl?.onFailure(response?.errmsg!!)
                    }
                }

            }
        } catch (e: Exception) {

        }
    }

    fun unLockViaGateway(lockId: Int) {
        lockDeatilsImpl?.onStarted()
        try {
            viewModelScope.launch {
                val response = repository.unLockViaGateway(lockId)
                response?.let {
                    if (response.errcode == 0) {
                        //lockDeatilsImpl?.onSuccess()
                       // getLockStatus()
                        lockDeatilsImpl?.onRemoteLockUnLockSuccess("UnLocked")
                    } else {
                        lockDeatilsImpl?.onFailure(response?.errmsg!!)
                    }
                }

            }
        } catch (e: Exception) {

        }
    }

    fun onClickStatusCheck(view: View) {
        if (lock?.hasGateway == 1 || lock?.remoteEnable == 1) {
            getLockStatus()
        }else{
            lockDeatilsImpl?.onFailure("Please check, Lock is not connected with Gateway.")
        }
    }

    private fun getLockStatus() {
        lockDeatilsImpl?.onStarted()
        try {
            viewModelScope.launch {
                val result = repository.getLockStatus(lock?.lockId!!)
                var message: String
                result.let {
                    if (it.state != null) {
                        message = if (it.state == 0) {
                            "Status : Lock"
                        } else {
                            "Status : Unlock"
                        }
                        lockDeatilsImpl?.onStatusUpdate(message)
                    } else {
                        lockDeatilsImpl?.onFailure(it?.errmsg!!)
                    }
                }


            }
        } catch (e: Exception) {

        }

    }

    fun getLockDetails(lockId: Int) {
        try {
            viewModelScope.launch {
                val result = repository.getLockDetails(lockId)
                Timber.e("Lock Details Response &s ", result)
            }
        } catch (ignored: Exception) {

        }
    }
}