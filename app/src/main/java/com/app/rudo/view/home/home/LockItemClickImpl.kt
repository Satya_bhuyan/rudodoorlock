package com.app.rudo.view.home.home

import android.view.View
import com.app.rudo.model.eaccess.EAccessKeyList

/*
// Created by Satyabrata Bhuyan on 05-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

interface LockItemClickImpl {
    fun itemClicked(view:View,lock:EAccessKeyList)
}