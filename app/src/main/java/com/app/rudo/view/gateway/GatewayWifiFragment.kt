package com.app.rudo.view.gateway

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.databinding.FragmentWifiListBinding
import com.app.rudo.model.gateway.AccList
import com.app.rudo.utils.AppPrefrences
import com.ttlock.bl.sdk.api.ExtendedBluetoothDevice
import com.ttlock.bl.sdk.gateway.api.GatewayClient
import com.ttlock.bl.sdk.gateway.callback.InitGatewayCallback
import com.ttlock.bl.sdk.gateway.model.ConfigureGatewayInfo
import com.ttlock.bl.sdk.gateway.model.DeviceInfo
import com.ttlock.bl.sdk.gateway.model.GatewayError
import com.ttlock.bl.sdk.util.LogUtil
import com.ttlock.bl.sdk.util.NetworkUtil
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import timber.log.Timber

class GatewayWifiFragment : Fragment(), KodeinAware ,GatewayImpl{
    override val kodein: Kodein by kodein()
    private val factory: GatewayViewmodelFactory by instance<GatewayViewmodelFactory>()
    private var viewModel: GatewayViewmodel? = null
    private var sharedPreferance: AppPrefrences? = null
    private var configureGatewayInfo: ConfigureGatewayInfo? = null
    private lateinit var binding: FragmentWifiListBinding
    private var deviceId: String? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_wifi_list, container, false)
        viewModel = ViewModelProvider(this, factory).get(GatewayViewmodel::class.java)
        GatewayClient.getDefault().prepareBTService(requireContext())
        return this.binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel?.registerListner(this)
        deviceId = arguments?.getString(ExtendedBluetoothDevice::class.java.name)
        sharedPreferance = AppPrefrences(requireContext())
        Timber.i("Data %s ", sharedPreferance?.getUserData()?.uid)
        configureGatewayInfo = ConfigureGatewayInfo()
        initView()
        initListener()
    }

    private fun initView() {
        if (NetworkUtil.isWifiConnected(requireContext())) {
            binding?.wifiName?.setText(NetworkUtil.getWifiSSid(requireContext()))
        }
    }

    private fun initListener() {
        binding?.btnInitGateway?.setOnClickListener {
            if (!TextUtils.isEmpty(binding?.wifiName?.text.toString())) {
                binding?.progressBar?.visibility = View.VISIBLE
                configureGatewayInfo?.uid = sharedPreferance?.getUserData()?.uid
                configureGatewayInfo?.userPwd = sharedPreferance?.getUserData()?.md5Pwd
                configureGatewayInfo?.ssid = binding?.wifiName?.text.toString().trim()
                configureGatewayInfo?.plugName = binding?.gatewayName.getText().toString().trim()
                configureGatewayInfo?.wifiPwd = binding?.wifiPwd?.text.toString().trim()

                //TODO:
                //configureGatewayInfo?.plugName = deviceId.toString()
                GatewayClient.getDefault().initGateway(configureGatewayInfo!!, object : InitGatewayCallback {
                    override fun onInitGatewaySuccess(deviceInfo: DeviceInfo) {
                        LogUtil.d("gateway init success")
                        //isInitSuccess(deviceInfo)
                        deviceId?.let {
                            binding?.progressBar?.visibility = View.INVISIBLE
                            viewModel?.initGateWay(deviceId!!, binding?.wifiName?.text.toString().trim(), deviceInfo)
                        }
                    }

                    override fun onFail(error: GatewayError) {
                        //view?.(error.description)
                        binding?.progressBar?.visibility = View.INVISIBLE
                        Timber.i("Error Message %s",error.toString())
                    }
                })
            }
        }
        // binding?.rlWifiName?.setOnClickListener({ v -> chooseWifiDialog() })
    }

    override fun onStarted() {

    }

    override fun onFailure(message: String) {

    }

    override fun OnGatewayAccItem(list: List<AccList>) {

    }

    override fun onGatewayAddSuccess() {
        view?.findNavController()?.popBackStack()
        view?.findNavController()?.navigate(R.id.gatewayAccFragment)
    }

    override fun onDeleteGatewaySuccess() {

    }
}