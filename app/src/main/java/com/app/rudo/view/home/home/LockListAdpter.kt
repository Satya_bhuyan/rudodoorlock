package com.app.rudo.view.home.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.databinding.CustomLockListBinding
import com.app.rudo.model.eaccess.EAccessKeyList

/*
// Created by Satyabrata Bhuyan on 04-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class LockListAdpter(
    val lockList: List<EAccessKeyList>,
    val lockItemClickImpl:LockItemClickImpl
):RecyclerView.Adapter<LockListAdpter.LockListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LockListViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.custom_lock_list,
                parent,
                false
            )
        )

    override fun getItemCount() = lockList.size

    override fun onBindViewHolder(holder: LockListViewHolder, position: Int) {
        holder.customLockListBinding.lock = lockList[position]
        holder.customLockListBinding.root.setOnClickListener {
            lockItemClickImpl.itemClicked(holder.customLockListBinding.root,lockList[position])
        }

    }
    inner class LockListViewHolder(
        val customLockListBinding: CustomLockListBinding
    ):RecyclerView.ViewHolder(customLockListBinding.root)
}