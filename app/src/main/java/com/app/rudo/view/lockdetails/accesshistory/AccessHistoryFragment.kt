package com.app.rudo.view.lockdetails.accesshistory

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.app.rudo.R
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.recodes.RecodeDetailsModel
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.view.lockdetails.LockDeatilsImpl
import com.app.rudo.view.lockdetails.LockDetailsViewModel
import com.app.rudo.view.lockdetails.LockDetailsViewmodelFactory
import com.app.rudo.view.lockdetails.iccard.SimpleDividerItemDecoration
import com.app.rudo.view.lockdetails.iccard.SwipeToDeleteCallback
import kotlinx.android.synthetic.main.fragment_access_history.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance


class AccessHistoryFragment : Fragment(), LockDeatilsImpl, KodeinAware,
    AccessHistoryAdapter.ItemClicked {

    override val kodein: Kodein by kodein()
    private val factory: LockDetailsViewmodelFactory by instance<LockDetailsViewmodelFactory>()
    var viewmodel: LockDetailsViewModel? = null
    var lockId: Int? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_access_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewmodel = ViewModelProvider(this, factory).get(LockDetailsViewModel::class.java)
        viewmodel?.registerListner(this)
        arguments?.let {
            lockId = it.getInt("lock")
            viewmodel?.getLockKey(lockId!!)
        }
        simpleSwipeRefreshLayout.setOnRefreshListener {
            viewmodel?.getLockKey(lockId!!)
        }
        // viewmodel.get
    }

    override fun onProgressHide() {
        progress_bar?.hide()
    }

    override fun onStarted() {
        progress_bar?.show()
    }

    override fun onSuccess() {
        progress_bar?.hide()
    }

    override fun onSuccesLockDetails(lockDetails: KeyData) {
        progress_bar?.hide()
    }

    override fun onFailure(message: String) {
        progress_bar?.hide()
    }


    override fun onSuccessLockKey(lockKeyList: List<EAccessKey>) {
        simpleSwipeRefreshLayout?.isRefreshing = false
        progress_bar?.hide()
        recycler_view?.also {
            if (lockKeyList.isNotEmpty()) {
                it.layoutManager = LinearLayoutManager(requireContext())
                it.setHasFixedSize(true)
                val reversedList = lockKeyList.reversed()
                it.adapter = AccessHistoryAdapter(reversedList as MutableList<EAccessKey>?, this)

                it.addItemDecoration(SimpleDividerItemDecoration(requireContext()))
                val swipeToDeleteCallback =
                    object : SwipeToDeleteCallback(
                        requireContext(),
                        0,
                        ItemTouchHelper.RIGHT
                    ) {
                        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                            (it.adapter as AccessHistoryAdapter).pendingRemoval(viewHolder.adapterPosition)
                        }

                        override fun getSwipeDirs(
                            recyclerView: RecyclerView,
                            viewHolder: RecyclerView.ViewHolder
                        ): Int {
                            if ((it.adapter as AccessHistoryAdapter).isPendingRemoval(viewHolder.adapterPosition)) {
                                return ItemTouchHelper.ACTION_STATE_IDLE
                            }
                            return super.getSwipeDirs(recyclerView, viewHolder)
                        }
                    }

                val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
                itemTouchHelper.attachToRecyclerView(it)
            }else{
                it.visibility = View.GONE
                flNoData.visibility = View.VISIBLE
            }
        }

    }

    override fun onSuccessPassCodes(lockKeyList: List<PassCodeListModel>) {

    }

    override fun onSuccessUnlockRecords(list: List<RecodeDetailsModel>) {

    }

    override fun onEAccessKeyDeleteSuccess() {

    }

    override fun onDeletePasscodeFromDetails(passcodeData: PassCodeListModel) {

    }

    override fun onStatusUpdate(status: String) {

    }

    override fun onRemoteLockUnLockSuccess(message: String) {

    }

    override fun deleteItem(eAccessKey: EAccessKey) {
        viewmodel?.deleteeAccessKey(eAccessKey)
    }

    override fun selectedItem(eAccessKey: EAccessKey) {
        val bundle = Bundle()
        bundle.putSerializable("eaccessdetails", eAccessKey)
        view?.findNavController()?.navigate(R.id.fragmentEAccessDetails, bundle)
    }
}