package com.app.rudo.view.lockdetails.iccard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.rudo.R
import com.app.rudo.model.locklist.Lock
import com.app.rudo.view.lockdetails.access.GiveAccessPageAdapter
import kotlinx.android.synthetic.main.fragment_give_access.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

/*
// Created by Satyabrata Bhuyan on 12-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class AddIcCardFragment : Fragment(), KodeinAware {

    private var lock: Lock? = null
    override val kodein: Kodein by kodein()
    var type:String? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_generate_passcode, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // lockId = arguments?.getInt("lock")
        arguments?.let {
            lock = (Lock::class.java).cast(arguments?.getSerializable("lockData"))
            type = arguments?.getString("type")
        }

        setPageAdpter()
    }

    private fun setPageAdpter() {
        val adpter = GiveAccessPageAdapter(childFragmentManager)
        adpter.addFragment(AddIcPermanentFragment(lock!!,type!!), "Permanent")
        adpter.addFragment(AddIcTimedFragment(lock!!,type!!), "Timed")
        viewpagerBase.adapter = adpter
    }
}