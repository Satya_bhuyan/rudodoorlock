package com.app.rudo.view.auth

import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.ActivityLoginNewBinding
import com.app.rudo.utils.*
import com.app.rudo.view.MainActivity
import kotlinx.android.synthetic.main.activity_login_new.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class LoginActivity : AppCompatActivity(), AuthListnerImpl, KodeinAware {

    override val kodein by kodein()
    private val factory: AuthViewModelFactory by instance<AuthViewModelFactory>()
    var viewmodel: AuthViewModel? = null
    private lateinit var pref : AppPrefrences
    @SuppressLint("ResourceAsColor")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityLoginNewBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_login_new)
        viewmodel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)
        binding.viewmodel = viewmodel
        viewmodel?.setListner(this)
        pref = AppPrefrences(this)
        //viewmodel?.userName = "9620218925"
        //viewmodel?.password = "123456"
        //viewmodel?.userName = "2233445566"
        // viewmodel?.password = "123456"
        //appPermission()
        val spnableString = SpannableString("Does not have an Account?\nSign up Here")
        spnableString.setSpan(
            ForegroundColorSpan(Color.BLUE),
            25,
            spnableString.length,
            Spanned.SPAN_EXCLUSIVE_INCLUSIVE
        )
        val clickableSpan: ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                navigateTo()
            }
        }
        spnableString.setSpan(clickableSpan, 25, spnableString.length, 0)
        text_view_sign_up.movementMethod = LinkMovementMethod.getInstance()
        text_view_sign_up.text = spnableString

       /* root_layout.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, m: MotionEvent): Boolean {
                // Perform tasks here
                flLogin.visibility = View.VISIBLE
                flResetPassword.visibility = View.GONE
                return true
            }
        })*/
        if(pref != null && !pref.getPermission()) {
            requestLocationWithDisclosure()
        }
    }

    override fun onStarted() {
        applicationContext.hideKeyboard(root_layout)
        progress_bar.show()
    }

    override fun onSuccess() {
        edit_text_email.text = null
        edit_text_password.text = null
        progress_bar.hide()
        Intent(this, MainActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(it)
            finish()
        }
        /*Intent(this, HomeActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(it)
            finish()
        }*/
    }

    override fun onForgetPasswordSuccess() {
        edit_text_email_forgetpassword.text = null
        edit_text_password_forgetpassword.text = null
        edit_text_password_confirm.text = null
       // button_sign_in.text = "Sign In"
       // viewmodel?.loginButtonText = "Sign In"
        progress_bar?.hide()
        root_layout.snackbar("Reset Password Successfully")
        flLogin.visibility = View.VISIBLE
        flResetPassword.visibility = View.GONE
    }

    override fun onFailure(message: String) {
        progress_bar.hide()
        root_layout.snackbar(message)
        //button_sign_in.text = "Sign In"
       // viewmodel?.loginButtonText = "Sign In"
    }

    override fun onClickForgetPassword() {
        edit_text_email.text = null
        edit_text_password.text = null
        // button_sign_in.text = "Reset"
        // viewmodel?.loginButtonText = "Reset"
        flLogin.visibility = View.GONE
        flResetPassword.visibility = View.VISIBLE

    }

    override fun navigateTo() {
        Intent(this, SignupActivity::class.java).also {
            it.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
            it.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            startActivity(it)
        }
    }

    override fun onLoginButtonClick() {
        viewmodel?.login()
        // viewmodel?.sendOtp(getString(R.string.otp_templete),"917899978035")
    }

    override fun otpGeneratedSuccess() {
        progress_bar?.hide()
    }

    override fun onForgotCancel() {
        flLogin.visibility = View.VISIBLE
        flResetPassword.visibility = View.GONE
    }

    /*private fun appPermission() {
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.SEND_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.SEND_SMS),
                REQUEST_PERMISSION_SMS
            )
            return
        }
        if (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_SMS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(Manifest.permission.READ_SMS),
                REQUEST_PERMISSION_SMS_READ
            )
            return
        }
    }*/

    fun requestLocationWithDisclosure(){
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.create()
        alertDialog.setTitle("Location Permission Disclosure")
        alertDialog.setMessage("This app collects location data to enable " +
                "BLE and Bluetooth Connection " +
                "even when the app is closed or not in use.” ")
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton("OK") { _, _ ->
            checkPermission()
        }
        alertDialog .show()
    }
    private fun checkPermission() {
        if (AppUtil.isAndroid12OrOver()) { //android 12 needs BLUETOOTH_SCAN permission
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_SCAN) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.BLUETOOTH_SCAN), AppUtil.REQUEST_PERMISSION_REQ_CODE)
                return
            }
        } else {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),AppUtil.REQUEST_PERMISSION_REQ_CODE)
                return
            }
        }
        // pref.savePermission(true)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if( grantResults[0] == PackageManager.PERMISSION_GRANTED ){
            pref.savePermission(true)
        }else if (grantResults[0] == PackageManager.PERMISSION_DENIED){
            pref.savePermission(false)
        }

    }
}
