package com.app.rudo.view.auth

import android.view.View
import android.widget.CheckBox
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.rudo.BuildConfig
import com.app.rudo.R
import com.app.rudo.model.AuthResponse
import com.app.rudo.repository.AuthRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import com.ttlock.bl.sdk.util.DigitUtil
import kotlinx.coroutines.launch
import timber.log.Timber


class AuthViewModel(
    private val repository: AuthRepository,
    private val prefrences: AppPrefrences
) : ViewModel() {

    private var authListnerImpl: AuthListnerImpl? = null
    var userName: String? = null//"8867830282"
    var password: String? = null//"pwd4TTLock"
    var confirmPassword: String? = null
    var forgetPasswordUserName:String? = null
    var forgetPassword:String? = null
    var eulaCheckBox = false
    var session_id:String? = null
    var _OTP:Any? = null
    var otp:String? = null
    var _tempForgetPasswordUserName:String ?= null
    //  fun getLoggedInUser() = repository.getUser()

    fun setListner(authListnerImpl: AuthListnerImpl) {
        this.authListnerImpl = authListnerImpl
    }

   /* fun onClickSignUp(view: View) {
        authListnerImpl?.navigateTo()
    }*/

    fun onClickSignIn(view: View) {
        authListnerImpl?.navigateTo()
    }

    fun onLoginButtonClick(view: View) {
        authListnerImpl?.onLoginButtonClick()


    }

    fun login() {
        if (userName.isNullOrEmpty()) {
            authListnerImpl?.onFailure("Invalid Mobile Number")
            return
        }
        if (password.isNullOrEmpty()) {
            authListnerImpl?.onFailure("Invalid PassWord")
            return
        }
        password = DigitUtil.getMD5(password!!.trim())
        authListnerImpl?.onStarted()
        try {
            viewModelScope.launch {
                val apiResponse = repository.userLogin(userName!!, password!!)
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            }
        } catch (e: ApiException) {
            authListnerImpl?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            authListnerImpl?.onFailure(e.message!!)
        }catch (e:Exception){

        }

    }



   /* private fun loginWithOpenTTServer() {
        viewModelScope.launch {
            try {
                val apiResponse = repository.userLoginWithOpenTT(userName!!, password!!)
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun loginWithYutuServer() {
        viewModelScope.launch {
            try {
                val apiResponse = repository.userLogin(userName!!, password!!)
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }*/

    private fun loginReponseHandle(response: AuthResponse) {
        if (response.errmsg.isNullOrEmpty()) {
            response.md5Pwd = password
            response.access_token?.let {
                prefrences?.saveAccessToken(it)
                prefrences?.saveUserInfo(userName!!)
                prefrences.saveUserData(response)
            }
            authListnerImpl?.onSuccess()

            //
        } else {
            authListnerImpl?.onFailure(response.errmsg)
        }
    }

    fun onSignUpButtonClick(view: View) {
        val context = view.context.applicationContext
        if (userName.isNullOrEmpty()) {
            authListnerImpl?.onFailure(context.getString(R.string.error_invalid_mobile))
            return
        }
        if (password.isNullOrEmpty()) {
            authListnerImpl?.onFailure(context.getString(R.string.error_invalid_password))
            return
        }
        if (password != confirmPassword) {
            authListnerImpl?.onFailure(context.getString(R.string.error_msg_passnotmatched))
            return
        }
        if(!eulaCheckBox){
            authListnerImpl?.onFailure("Please check End User License Agreement")
            return
        }
        authListnerImpl?.onStarted()
        password = DigitUtil.getMD5(password)
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            signUpWithOpenTTServer(view)
        } else {
            signUpWithYutuServer(view)
        }
    }

    private fun signUpWithOpenTTServer(view: View) {
        viewModelScope.launch {
            try {
                val apiResponse =
                    repository.userSignupWithOpenTT(
                        userName!!,
                        password!!,
                        System.currentTimeMillis()
                    )
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun signUpWithYutuServer(view: View) {
        viewModelScope.launch {
            try {
                val apiResponse =
                    repository.userSignup(userName!!, password!!, System.currentTimeMillis())
                apiResponse?.let {
                    loginReponseHandle(it)
                }
            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    fun forgetPasswordClick(view: View) {
       // loginButtonText = "Reset"
        authListnerImpl?.onClickForgetPassword()
    }
    fun onResetPasswordButtonClick(view: View){
        val context = view.context.applicationContext
        if (forgetPasswordUserName.isNullOrEmpty() || forgetPasswordUserName != _tempForgetPasswordUserName ) {
            authListnerImpl?.onFailure(context.getString(R.string.error_invalid_mobile))
            return
        }
        if (forgetPassword.isNullOrEmpty()) {
            authListnerImpl?.onFailure(context.getString(R.string.invalid_password))
            return
        }
        if(forgetPassword != confirmPassword){
            authListnerImpl?.onFailure(context.getString(R.string.error_msg_passwordmismatched))
            return
        }
        if(otp.isNullOrEmpty() || otp != _OTP.toString()){
            authListnerImpl?.onFailure(context.getString(R.string.error_invalid_otp))
            return
        }
        /*if (session_id.isNullOrEmpty()){
            authListnerImpl?.onFailure(context.getString(R.string.error_invalid_otp_session))
            return
        }*/
        authListnerImpl?.onStarted()
        viewModelScope.launch {
            try {
                //val verifyOtp = repository.verifyOtp(session_id!!,otp!!)
               // verifyOtp.let {
                   // if (it.Status == "Success") {
                        val apiRequest = repository.forgetPassword(
                            forgetPasswordUserName!!,
                            DigitUtil.getMD5(forgetPassword!!.trim())
                        )
                        apiRequest.let {
                            forgetPasswordResponsehandle(it)
                        }
                   // }
               // }

            } catch (e: ApiException) {
                authListnerImpl?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                authListnerImpl?.onFailure(e.message!!)
            }
        }
    }

    private fun forgetPasswordResponsehandle(authResponse: AuthResponse) {
        if (authResponse.errcode == 0) {
            authListnerImpl?.onForgetPasswordSuccess()
        } else {
            authResponse.errcode.let {
                authListnerImpl?.onFailure(authResponse.errmsg!!)
            }
        }
    }

    fun sendOtp(number:String){
        try {
            viewModelScope.launch {
               val response = repository.sendOtp(number)
                response.let {
                    Timber.d("response %s",response.toString())
                }
            }

        }catch (e:Exception){

        }
    }
    fun verifyOtp(otp:String,session_id:String){
        try {
            viewModelScope.launch {
                val response = repository.verifyOtp(session_id,otp)
                response.let {
                    Timber.d("response %s",response.toString())
                }
            }

        }catch (e:Exception){

        }
    }
    // suspend fun saveLoggedInUser(user: User) = repository.saveUser(user)
    fun onClickChekBox(view: View){
        val ch = view as CheckBox
        eulaCheckBox = ch.isChecked
    }

    fun onClickGeneratePassword(view: View){
        val context = view.context.applicationContext

        if (forgetPasswordUserName.isNullOrEmpty()) {
            authListnerImpl?.onFailure(context.getString(R.string.error_invalid_mobile))
            return
        }
        authListnerImpl?.onStarted()
        viewModelScope.launch {
            try{
               _tempForgetPasswordUserName =  forgetPasswordUserName
                val response  = repository.sendOtp(forgetPasswordUserName!!)
                //val gson = Gson().toJson(response.toString())
               // val res = Gson().fromJson(gson,ApiResponseModel::class.java)
                Timber.d("res :",response.toString())
                response.let {
                    if (it.OTP != null ){
                        _OTP  = it.OTP
                        authListnerImpl?.otpGeneratedSuccess()
                    }else{
                        authListnerImpl?.onFailure("Please Retry")
                    }
                }
            }catch (e: ApiException){
                authListnerImpl?.onFailure("Please Retry")
            }
            catch (e:Exception){

            }
        }
    }
    fun onCancelForgotPassword(view: View){
        authListnerImpl?.onForgotCancel()
    }
}