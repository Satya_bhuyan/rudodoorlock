package com.app.rudo.view.home.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.repository.HomeRepository
import com.app.rudo.utils.AppPrefrences

/*
// Created by Satyabrata Bhuyan on 02-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class HomeViewmodelFactory(
    private val repository: HomeRepository,
private val pref:AppPrefrences
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return HomeViewModel(repository,pref) as T
    }
}