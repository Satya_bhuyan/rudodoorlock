package com.app.rudo.view.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.R
import com.app.rudo.databinding.FragmentAccountBinding
import com.app.rudo.utils.AppPrefrences
import kotlinx.android.synthetic.main.fragment_account.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class AccountFragment : Fragment(), KodeinAware {
    override val kodein: Kodein by kodein()
    private val factory: AccountViewModelFactory by instance<AccountViewModelFactory>()
    var pref: AppPrefrences? = null
    private lateinit var viewModel: AccountViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding: FragmentAccountBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_account, container, false)
        viewModel = ViewModelProvider(this, factory).get(AccountViewModel::class.java)
        binding.accountVm = viewModel
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref = AppPrefrences(requireContext())
        pref?.let {
            mobileNumber.text = pref?.getUserInfo()
            name.text = pref?.getName()
        }
    }
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(AccountViewModel::class.java)
        // TODO: Use the ViewModel
    }

}