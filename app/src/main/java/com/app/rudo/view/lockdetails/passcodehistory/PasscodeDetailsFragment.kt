package com.app.rudo.view.lockdetails.passcodehistory

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.databinding.FragmentPasscodeItemDetailsBindingImpl
import com.app.rudo.model.PassCodeListModel
import com.app.rudo.model.ekeys.EAccessKey
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.locklist.Lock
import com.app.rudo.model.recodes.RecodeDetailsModel
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import com.app.rudo.utils.toast
import com.app.rudo.view.lockdetails.LockDeatilsImpl
import com.app.rudo.view.lockdetails.LockDetailsViewModel
import com.app.rudo.view.lockdetails.LockDetailsViewmodelFactory
import com.ttlock.bl.sdk.api.TTLockClient
import com.ttlock.bl.sdk.callback.DeletePasscodeCallback
import com.ttlock.bl.sdk.entity.LockError
import kotlinx.android.synthetic.main.fragment_passcode_item_details.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 11-08-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class PasscodeDetailsFragment : Fragment(), KodeinAware,LockDeatilsImpl{

    override val kodein: Kodein by kodein()
    private val factory: LockDetailsViewmodelFactory by instance<LockDetailsViewmodelFactory>()
    var viewmodel: LockDetailsViewModel? = null
    var passcodeModel : PassCodeListModel? = null
    var lock:Lock? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        // Inflate the layout for this fragment
        val binding: FragmentPasscodeItemDetailsBindingImpl =
            DataBindingUtil.inflate(inflater, R.layout.fragment_passcode_item_details, container, false)
        viewmodel = ViewModelProvider(this, factory).get(LockDetailsViewModel::class.java)
        viewmodel?.registerListner(this)
        arguments?.let {
            passcodeModel = (PassCodeListModel::class.java).cast(arguments?.getSerializable("passcodedetails"))
            lock = (Lock::class.java).cast(arguments?.getSerializable("lockData"))
            viewmodel?.passCodeListModel = passcodeModel
            binding.passviewmodel = viewmodel
            binding.passdetails = passcodeModel
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imgShare.setOnClickListener {
            if (!TextUtils.isEmpty(passCode.text.toString())) {
                val sendIntent = Intent()
                sendIntent.setAction(Intent.ACTION_SEND)
                // sendIntent.setPackage("com.whatsapp")
                sendIntent.putExtra(Intent.EXTRA_TEXT, passCode.text.toString())
                sendIntent.setType("text/plain")
                startActivity(sendIntent)
            }
        }
    }
    override fun onProgressHide() {

    }

    override fun onStarted() {
        progress_bar?.show()
    }

    override fun onSuccess() {

    }

    override fun onSuccesLockDetails(lockDetails: KeyData) {

    }

    override fun onFailure(message: String) {
        progress_bar?.hide()
    }

    override fun onSuccessLockKey(lockKeyList: List<EAccessKey>) {

    }

    override fun onSuccessPassCodes(lockKeyList: List<PassCodeListModel>) {
        progress_bar?.hide()
       view?.findNavController()?.popBackStack()
    }

    override fun onSuccessUnlockRecords(list: List<RecodeDetailsModel>) {

    }

    override fun onEAccessKeyDeleteSuccess() {

    }

    override fun onDeletePasscodeFromDetails(passcodeData: PassCodeListModel) {
        onStarted()
        TTLockClient.getDefault().deletePasscode(passcodeData.keyboardPwd,
            lock?.lockData,
            lock?.lockMac,
            object : DeletePasscodeCallback {
                override fun onFail(error: LockError?) {
                    if (lock?.hasGateway != null && lock?.hasGateway == 1){
                        viewmodel?.deletePasscode(passcodeData,2)
                    }else if (error?.name == "LOCK_CONNECT_FAIL") {
                        progress_bar?.hide()
                        requireContext().toast(requireContext().getString(R.string.bluetooth_operation_failed))
                    }
                }

                override fun onDeletePasscodeSuccess() {
                    viewmodel?.deletePasscode(passcodeData,1)
                }

            })
    }

    override fun onStatusUpdate(status: String) {

    }

    override fun onRemoteLockUnLockSuccess(message: String) {
        
    }
}