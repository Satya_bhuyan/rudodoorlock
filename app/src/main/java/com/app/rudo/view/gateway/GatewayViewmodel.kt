package com.app.rudo.view.gateway

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.repository.GatewayRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import com.ttlock.bl.sdk.gateway.model.DeviceInfo
import kotlinx.coroutines.launch

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GatewayViewmodel(
    private val repository: GatewayRepository,
    private val pref: AppPrefrences
) : ViewModel() {

    private var _gatewayId: Int? = null
    var listner: GatewayImpl? = null
    fun registerListner(mListner: GatewayImpl) {
        this.listner = mListner
    }

    fun setGatewayID(gatewayId: Int) {
        _gatewayId = gatewayId
    }

    fun getAccList() {
        getGatewayAccLisYutu()
    }

    fun OnClickAddButton(view: View) {
        view.findNavController().navigate(R.id.action_gatewayacc_to_addgateway)
    }

    private fun getGatewayAccLisYutu() {
        listner?.onStarted()
        try {
            viewModelScope.launch {
                val response = repository.getGatewayAccList()
                response?.let {
                    if (response.errcode == null) {
                        listner?.OnGatewayAccItem(response.list)
                    } else {
                        listner?.onFailure(response.errmsg!!)
                    }
                }
            }
        } catch (e: ApiException) {
            listner?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            listner?.onFailure(e.message!!)
        }
    }

    fun initGateWay(getewayNetMac: String, networkName: String, deviceInfo: DeviceInfo) {
        try {
            viewModelScope.launch {
                val response = repository.initGateway(getewayNetMac)
                response?.let {
                    if (response.errcode == 0 && response.gatewayId != null)
                        uploadGatwayData(deviceInfo, response.gatewayId, networkName)
                }
            }
        } catch (e: Exception) {

        }
    }

    private fun uploadGatwayData(deviceInfo: DeviceInfo, gatewayId: Int, networkName: String) {
        try {
            viewModelScope.launch {
                val response = repository.uploadGatewayDetails(
                    gatewayId,
                    deviceInfo.modelNum,
                    deviceInfo.hardwareRevision,
                    deviceInfo.firmwareRevision,
                    networkName
                )
                response?.let {
                    if (response.errcode == 0)
                        listner?.onGatewayAddSuccess()
                }
            }
        } catch (e: Exception) {

        }
    }

    fun deleteGateway(view: View) {
        try {
            viewModelScope.launch {
                val response = repository.deleteGateWay(_gatewayId!!)
                response?.let { res ->
                    if (res.errcode == 0) {
                        listner?.onDeleteGatewaySuccess()
                    }
                }
            }
        } catch (ignored: Exception) {

        }
    }

}