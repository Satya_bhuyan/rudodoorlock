package com.app.rudo.view.gateway

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.databinding.FragmentGatewayItemDetailsBinding
import com.app.rudo.model.gateway.AccList
import com.app.rudo.model.gateway.GatewayItemDetails
import com.app.rudo.utils.hide
import com.app.rudo.utils.show
import kotlinx.android.synthetic.main.fragment_gateway_item_details.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 20-05-2021.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GatewayItemDetailsFragment :Fragment() ,KodeinAware,GatewayImpl {
    override val kodein: Kodein by kodein()
    private val factory: GatewayViewmodelFactory by instance<GatewayViewmodelFactory>()
    private var viewModel: GatewayViewmodel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding: FragmentGatewayItemDetailsBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_gateway_item_details, container, false)
        viewModel = ViewModelProvider(this, factory).get(GatewayViewmodel::class.java)
        binding.gatewayItem = viewModel
        arguments?.let {
            val data = it.getSerializable("gatewayData")
            binding.gatewaydetails =data as GatewayItemDetails
            viewModel?.setGatewayID(data?.gatewayId!!)
        }

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel?.registerListner(this)
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun onStarted() {
        progress_bar?.show()
    }

    override fun onFailure(message: String) {
        progress_bar?.hide()
    }

    override fun OnGatewayAccItem(list: List<AccList>) {

    }

    override fun onGatewayAddSuccess() {

    }

    override fun onDeleteGatewaySuccess() {
        view?.findNavController()?.popBackStack()
    }
}