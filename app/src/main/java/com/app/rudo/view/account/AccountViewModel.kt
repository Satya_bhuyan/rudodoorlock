package com.app.rudo.view.account

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.repository.AuthRepository
import com.app.rudo.utils.AppPrefrences
import kotlinx.coroutines.launch

class AccountViewModel(
    private val repository: AuthRepository,
    private val pref: AppPrefrences
) : ViewModel() {
    var currentPassword: String? = null
    var newPassword: String? = null
    var confirmPassword: String? = null
    private var listner: AccountImple? = null

    fun registerListner(impl: AccountImple) {
        listner = impl
    }

    fun OnClickNickName(view: View) {
        view?.findNavController()?.navigate(R.id.userAccountRename)
    }

    fun OnClickEmail(view: View) {
        view?.findNavController()?.navigate(R.id.accountEmail)
    }

    fun OnClickResetPassword(view: View) {
        view?.findNavController()?.navigate(R.id.accountResetPassword)
    }

    fun OnClickSecurityQuestion(view: View) {

    }

    fun OnClickResetPasswordSave(view: View) {
        val context = view.context.applicationContext
        if (currentPassword.isNullOrEmpty()) {
            listner?.onError(context.getString(R.string.error_msg_currentpassword))
            return
        }
        if (newPassword.isNullOrEmpty()) {
            listner?.onError(context.getString(R.string.error_msg_newpassword))
            return
        }
        if (confirmPassword.isNullOrEmpty()) {
            listner?.onError(context.getString(R.string.error_msg_confirmpassword))
            return
        }
        if (currentPassword == newPassword) {
            listner?.onError(context.getString(R.string.error_msg_passnotmatched))
            return
        }
        if (newPassword != confirmPassword) {
            listner?.onError(context.getString(R.string.error_msg_passwordmismatched))
            return
        }
        viewModelScope.launch {
            val response = repository.forgetPassword(pref?.getUserInfo()!!, newPassword!!)
            response?.let {
                if (it.errcode == 0) {
                    listner?.onSuccess()
                } else {
                    listner?.onError(it.errmsg!!)
                }
            }
        }
    }
}