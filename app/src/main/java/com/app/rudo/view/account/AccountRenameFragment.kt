package com.app.rudo.view.account

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.hideKeyboard
import kotlinx.android.synthetic.main.fragment_account_rename.*

class AccountRenameFragment : Fragment() {
    var pref: AppPrefrences? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_account_rename, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pref = AppPrefrences(requireContext())
        name.setText(pref?.getName())
        buttonSave.setOnClickListener {
            if(!TextUtils.isEmpty(name.text.toString())) {
                context?.hideKeyboard(view?.rootView!!)
                pref?.setName(name.text.toString())
                view?.findNavController().popBackStack()
            }

        }
    }
}