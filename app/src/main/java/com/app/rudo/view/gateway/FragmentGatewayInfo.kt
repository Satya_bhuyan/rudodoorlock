package com.app.rudo.view.gateway

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.utils.showAlertDialog
import com.ttlock.bl.sdk.util.NetworkUtil
import kotlinx.android.synthetic.main.fragment_gateway_add_info.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/
class FragmentGatewayInfo : Fragment(), KodeinAware {
    override val kodein: Kodein by kodein()
    private var dialog: AlertDialog? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_gateway_add_info,container,false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        optionClick()
    }
    fun optionClick(){
        buttonNext.setOnClickListener {
            if (NetworkUtil.isWifiConnected(requireContext())) {
                view?.findNavController()?.navigate(R.id.addGatewayFragment)
            }else{
                dialog = showAlertDialog {
                    txtMessage.visibility = View.GONE
                    rlHeader.visibility = View.GONE
                    eText.text = "Please connect your phone to wifi network where you wants connect your Gateway."
                    btnClickListener {
                        dialog?.cancel()
                    }
                }
                dialog?.setCancelable(false)
                dialog?.show()
                //requireContext().toast("Please connect your phone to wifi network where your Gateway wants to connect")
            }
        }
    }
}