package com.app.rudo.view.lockdetails.access

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.app.rudo.R
import com.app.rudo.contrants.Constants
import com.app.rudo.databinding.FragmentGiveTimedBinding
import com.app.rudo.model.locklist.Lock
import com.app.rudo.utils.*
import kotlinx.android.synthetic.main.fragment_give_timed.*
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

/*
// Created by Satyabrata Bhuyan on 06-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GiveTimedFragment(private var lock: Lock): Fragment(),GiveAccessImpl, KodeinAware,DialogUitls.OnClickDialogItemImpl,
DateUtils.Companion.DateTimePickerImpl{
    override val kodein: Kodein by kodein()
    private val factory:GiveAccessViewModelFactory by instance<GiveAccessViewModelFactory>()
    private var viewModel:GiveAccessViewModel? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding : FragmentGiveTimedBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_give_timed,
            container,
            false
        )
        viewModel = ViewModelProvider(this,factory).get(GiveAccessViewModel::class.java)
        viewModel?.registerListner(this)
        binding.timed = viewModel
        viewModel?.registerListner(this)
        viewModel?.lockId = lock.lockId
        viewModel?.lock = lock
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val startTime =DateUtils.getCurrentDateTime("startTime")
        val endTime =DateUtils.getCurrentDateTime("endTime")
        stateTimeTimed.text = DateUtils.getFormatedDateAndTime(startTime)
        endDateTimed.text = DateUtils.getFormatedDateAndTime(endTime)
        viewModel?.startTime = startTime
        viewModel?.endTime = endTime
        stateTimeTimed.setOnClickListener{
            DateUtils.getDatePicker(requireContext(),Constants.START_TIME,this)
        }
        endDateTimed.setOnClickListener{
            DateUtils.getDatePicker(requireContext(),Constants.END_TIME,this)
        }
        authorizedAdmin.setOnCheckedChangeListener { buttonView, isChecked ->
            viewModel?.authorizedAdmin = isChecked
        }
        if (lock?.userType != null && Constants.COMMON_KEY == lock.userType) {
            authAdminLinerlayout.visibility = View.GONE
            authViewLine.visibility = View.GONE
        }
        swRemoteUnlock?.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked){
                viewModel?.remoteAllow = 1
            }else{
                viewModel?.remoteAllow = 2
            }
        }
    }

    override fun onStarted() {
        context?.hideKeyboard(view?.rootView!!)
        progressBarGiveTimed.show()
    }

    override fun onSuccess() {
        edtRecipients.setText("")
        edtEkeyName.setText("")
        progressBarGiveTimed.hide()
        DialogUitls.showDialog(
            requireContext(),
            "",
            "eAccess shared successfully",
            true,
            false,
            true,
            this
        )
    }

    override fun onFailure(message: String) {
        progressBarGiveTimed.hide()
        view?.rootView?.snackbar(message)
    }

    override fun onTimeError() {
        requireContext().toast(getString(R.string.time_error))
    }

    override fun onClickYes(dialog: Dialog) {
        dialog?.dismiss()
        view?.findNavController()?.popBackStack()
    }

    override fun onClickNo(dialog: Dialog) {

    }

    override fun onClickDialogDelete(password: String) {

    }


    override fun onClickCancel(dialog: Dialog) {

    }

    override fun onRemoteUnlock(dialog: Dialog) {

    }

    override fun setDateTime(millsec: Long,identifyer:String) {
        val temp = DateUtils.getFormatedDateAndTime(millsec)
        if(Constants.START_TIME.equals(identifyer,ignoreCase = true)){
            stateTimeTimed.text = temp
            viewModel?.startTime = millsec
        }else if(Constants.END_TIME.equals(identifyer,ignoreCase = true)){
            viewModel?.endTime = millsec
            endDateTimed.text = temp
        }
    }
}