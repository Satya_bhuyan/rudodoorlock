package com.app.rudo.view.lockdetails.passcode

import android.view.View
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.rudo.BuildConfig
import com.app.rudo.model.locklist.Lock
import com.app.rudo.repository.LockDetailRepository
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.NoInternetException
import kotlinx.coroutines.launch

class GeneratePasscodeViewModel(
    private val repository: LockDetailRepository
) : ViewModel() {
    //var keyData: KeyData? = null
    var lock: Lock? = null
    var recipient: String? = null
    var name: String? = null
    var startTime: Long = 0
    var endTime: Long = 0
    var remoteAllow: Int = 2
    private var keyboardPwdType: Int? = null
    private var listner: GeneratePasscodeImpl? = null


    fun registerListner(Impl: GeneratePasscodeImpl) {
        listner = Impl
    }

    fun setkeyboardPwdType(keyboardPwdType: Int) {
        this.keyboardPwdType = keyboardPwdType
    }

    fun onClickSwitAllow(view: View) {
        remoteAllow = if (view.isSelected) {
            1
        } else {
            2
        }
    }

    fun onClickSwitAuthorized(view: View) {

    }

    fun onClickSendButton(view: View) {

        if (name.isNullOrEmpty()) {
            listner?.onFailure("Name is empty")
            return
        }
        if(endTime < startTime){
            listner?.onTimeError()
            return
        }
        /* if (recipient.isNullOrEmpty()) {
             listner?.onFailure("Recipient is empty")
             return
         }*/
        if (keyboardPwdType == null) {
            return
        }
        listner?.onStarted()
        getPasscode()
    }


    fun getPasscode() {
        try {
            viewModelScope.launch {
                val response = repository.getPassCode(
                    lock?.lockId!!,
                    keyboardPwdType!!,
                    name!!,
                    startTime,
                    endTime
                )
                response.let {
                    it.keyboardPwd?.let {
                        listner?.onSuccess(it)
                    }
                    it.errmsg?.let {
                        listner?.onFailure(it)
                    }
                }
            }
        } catch (e: ApiException) {
            listner?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            listner?.onFailure(e.message!!)
        }
    }

    fun addPassCode() {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            addPasscodeOpenTTApi()
        } else {
            addPasscodeYutu()
        }
    }

    private fun addPasscodeYutu() {
        viewModelScope.launch {
            try {
                val response = repository.addPassCode(
                    lock?.lockId!!,
                    lock?.noKeyPwd!!,
                    name!!,
                    startTime,
                    endTime,
                    1
                )
                response?.let {
                    it.keyboardPwdId.let {
                        listner?.onSuccess(it.toString())
                    }
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }

    private fun addPasscodeOpenTTApi() {
        viewModelScope.launch {
            try {
                val response = repository.addPassCodeOpenTTApi(
                    lock?.lockId!!,
                    lock?.noKeyPwd!!,
                    name!!,
                    startTime,
                    endTime,
                    1
                )
                response?.let {
                    it.keyboardPwdId.let {
                        listner?.onSuccess(it.toString())
                    }
                }
            } catch (e: ApiException) {
                listner?.onFailure(e.message!!)
            } catch (e: NoInternetException) {
                listner?.onFailure(e.message!!)
            }
        }
    }
    fun addCustomPasscode(passcode:String,name:String){
        listner?.onStarted()
        try {
            viewModelScope.launch {
                val response = repository.addCustomPasscode(lock?.lockId!!,passcode,name,startTime,endTime)
                response.let {
                    listner?.onSuccess(passcode)
                }
            }
        } catch (e: ApiException) {
            listner?.onFailure(e.message!!)
        } catch (e: NoInternetException) {
            listner?.onFailure(e.message!!)
        }
    }
}