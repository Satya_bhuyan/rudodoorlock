package com.app.rudo.view.gateway

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.rudo.repository.GatewayRepository
import com.app.rudo.utils.AppPrefrences

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GatewayViewmodelFactory(
    private val repository: GatewayRepository,
    private val pref: AppPrefrences
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return GatewayViewmodel(repository, pref) as T
    }
}