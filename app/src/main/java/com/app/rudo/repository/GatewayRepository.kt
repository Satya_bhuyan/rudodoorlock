package com.app.rudo.repository

import com.app.rudo.BuildConfig
import com.app.rudo.model.ApiResponseModel
import com.app.rudo.model.gateway.GatewayAccList
import com.app.rudo.network.ApiRequest
import com.app.rudo.network.api.RudoApi
import com.app.rudo.utils.AppPrefrences
import kotlinx.coroutines.CompletableJob

/*
// Created by Satyabrata Bhuyan on 23-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class GatewayRepository(
    private val rudoApi: RudoApi,
    private val appPrefrences: AppPrefrences
) : ApiRequest() {

    var job: CompletableJob? = null

    suspend fun getGatewayAccList(): GatewayAccList {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            var result = apiRequest {
                rudoApi.getGatewayAccountListOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    1,
                    20,
                    System.currentTimeMillis()
                )!!
            }
            result.let { data ->
                data.list?.forEach {
                    if (it.isOnline == 1) {
                        it.status = "Online"
                    } else {
                        it.status = "Offline"
                    }
                }
            }
            return result
        } else {
            return apiRequest {
                rudoApi.getGatewayAccountList(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    1,
                    20,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/gateway/list"
                )!!
            }
        }

    }

    suspend fun uploadGatewayDetails(
        getewayId: Int, modelNum: String,
        hardwareRevision: String, firmwareRevision: String, networkName: String
    ): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.uploadGatewayDetailsOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    getewayId,
                    modelNum,
                    hardwareRevision,
                    firmwareRevision,
                    networkName,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.uploadGatewayDetails(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    getewayId,
                    modelNum,
                    hardwareRevision,
                    firmwareRevision,
                    networkName,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/gateway/uploadDetail"
                )!!
            }
        }
    }

    suspend fun initGateway(getewayNetMac: String): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.initGatewayOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    getewayNetMac,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.initGateway(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    getewayNetMac,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/gateway/isInitSuccess"
                )!!
            }
        }
    }

    suspend fun deleteGateWay(gatewayId: Int): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.deleteGatewayOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    gatewayId,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.deleteGateway(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    gatewayId,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/gateway/delete"
                )!!
            }
        }


    }

    fun cancelJobs() {
        job?.cancel()
    }
}