package com.app.rudo.repository

import com.app.rudo.BuildConfig
import com.app.rudo.contrants.Constants
import com.app.rudo.model.ApiResponseModel
import com.app.rudo.model.LockStatus
import com.app.rudo.model.PasscodeMainModel
import com.app.rudo.model.PasscodeModel
import com.app.rudo.model.ekeys.EAccessKeysList
import com.app.rudo.model.fingerprint.FingerprintList
import com.app.rudo.model.iccard.IccardList
import com.app.rudo.model.lockdetails.KeyData
import com.app.rudo.model.lockdetails.LockDetails
import com.app.rudo.model.recodes.RecordListModel
import com.app.rudo.network.ApiRequest
import com.app.rudo.network.api.RudoApi
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.DateUtils
import com.app.rudo.utils.NoInternetException
import java.net.URLDecoder

/*
// Created by Satyabrata Bhuyan on 05-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

@Suppress("UNREACHABLE_CODE")
class LockDetailRepository(
    private val rudoApi: RudoApi,
    private val appPrefrences: AppPrefrences
) : ApiRequest() {

    suspend fun getLockDetails(lockId: Int): LockDetails {

        val result: LockDetails? = if (BuildConfig.IS_OPENTT_URL_CALL) {
            apiRequest {
                rudoApi.getLockDetailsFromOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            apiRequest {
                rudoApi.getLockDetails(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/detail"
                )!!
            }
        }
        result?.lockAlias?.let {
            result.lockAlias = URLDecoder.decode(result.lockAlias)
        }

        return result!!
    }

    suspend fun getLockDetailsFromOpenTTApi(lockId: Int): LockDetails {
        val result = apiRequest {
            rudoApi.getLockDetailsFromOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis()
            )!!
        }
        result?.let {
            it.lockAlias?.let { result.lockAlias = URLDecoder.decode(result?.lockAlias) }

        }
        return result
    }

    suspend fun getKeyList(lockId: Int): KeyData {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.getKeyListFromOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.getKeyList(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/key/get"
                )!!
            }
        }
    }

    /* suspend fun getKeyListFromOpenTTApi(lockId: Int): KeyData {
         return apiRequest {
             rudoApi.getKeyListFromOpenTTApi(
                 BuildConfig.CLIENT_ID,
                 appPrefrences.getAccessToken(),
                 lockId,
                 System.currentTimeMillis()
             )!!
         }
     }*/

    suspend fun sendAccessKey(
        lockId: Int,
        receverName: String,
        name: String,
        stateDate: Long,
        endDate: Long,
        remoteEnabled: Int,
        remarks: String
    ): ApiResponseModel {

        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.sendeAccessKeyToOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    receverName,
                    name,
                    stateDate,
                    endDate,
                    remarks,
                    remoteEnabled,
                    1,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.sendeAccessKey(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    receverName,
                    name,
                    stateDate,
                    endDate,
                    remarks,
                    remoteEnabled,
                    1,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/key/send"
                )!!
            }
        }
    }


    suspend fun addPassCode(
        lockId: Int,
        keyboardPwd: String,
        keyboardPwdName: String,
        startTimeDate: Long,
        endTimeDate: Long,
        addType: Int

    ): PasscodeModel {
        return apiRequest {
            rudoApi.addPassCode(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                keyboardPwd,
                keyboardPwdName,
                startTimeDate,
                endTimeDate,
                System.currentTimeMillis(),
                addType,
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/keyboardPwd/add"
            )!!
        }
    }

    suspend fun addPassCodeOpenTTApi(
        lockId: Int,
        keyboardPwd: String,
        keyboardPwdName: String,
        startTimeDate: Long,
        endTimeDate: Long,
        addType: Int

    ): PasscodeModel {
        return apiRequest {
            rudoApi.addPassCodeOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                keyboardPwd,
                keyboardPwdName,
                startTimeDate,
                endTimeDate,
                System.currentTimeMillis(),
                addType
            )!!
        }
    }

    suspend fun getPassCode(
        lockId: Int,
        keyboardPwdType: Int,
        keyboardPwdName: String,
        startTimeDate: Long,
        endTimeDate: Long
    ): PasscodeModel {

        if (BuildConfig.IS_OPENTT_URL_CALL){
            return apiRequest {
                rudoApi.getPassCodeOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    4,
                    keyboardPwdType,
                    keyboardPwdName,
                    startTimeDate,
                    endTimeDate,
                    System.currentTimeMillis()
                )!!
            }
        }else{
            return apiRequest {
                rudoApi.getPassCode(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    4,
                    keyboardPwdType,
                    keyboardPwdName,
                    startTimeDate,
                    endTimeDate,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/keyboardPwd/get"
                )!!
            }
        }

    }

    suspend fun getLockKey(lockId: Int): EAccessKeysList {

        if (BuildConfig.IS_OPENTT_URL_CALL) {
            val result = apiRequest {
                rudoApi.getLockKeyListOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    1,
                    20,
                    System.currentTimeMillis()
                )!!
            }
            result.let {

                it.list?.forEach {
                    if (it.startDate == 0L && it.endDate == 0L) {
                        it.type = "Permanent"
                    } else if (it.remarks != "") {
                        it.type = "One Time"
                    } else {
                        it.type =
                            DateUtils.getFormatedDateAndTime(it.startDate) + " -" + DateUtils.getFormatedDateAndTime(
                                it.endDate
                            )
                    }

                }
                it.list?.filter { it.username!!.isNotEmpty() }?.forEach {
                    if (it.username!!.contains("_")) {
                        it.username = it.username!!.split("_")[1]
                    }
                }
                it.list?.filter { it.senderUsername!!.isNotEmpty() }?.forEach {
                    if (it.senderUsername!!.contains("_")) {
                        it.senderUsername = it.senderUsername!!.split("_")[1]
                    }
                }
            }
            return result
        } else {
            val result = apiRequest {
                rudoApi.getLockKeyList(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    1,
                    20,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/listKey"
                )!!
            }
            result.let {
                it.list?.forEach {
                    if (it.startDate == 0L && it.endDate == 0L) {
                        it.type = "Permanent"
                    } else {
                        it.type =
                            DateUtils.getFormatedDateAndTime(it.startDate) + " -" + DateUtils.getFormatedDateAndTime(
                                it.endDate
                            )
                    }

                }
                it.list?.filter { it.username!!.isNotEmpty() }?.forEach {
                    if (it.username!!.contains("_")) {
                        it.username = it.username!!.split("_")[1]
                    }
                }
                it.list?.filter { it.senderUsername!!.isNotEmpty() }?.forEach {
                    if (it.senderUsername!!.contains("_")) {
                        it.senderUsername = it.senderUsername!!.split("_")[1]
                    }
                }
            }
            return result
        }
    }

    suspend fun getPassCodes(lockId: Int): PasscodeMainModel {

        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.getPassCodeListOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    1,
                    20,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.getPassCodeList(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    1,
                    20,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/listKeyboardPwd"
                )!!
            }
        }

    }

    suspend fun getICCardList(lockId: Int): IccardList {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.getICCardListOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    1,
                    20,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.getICCardList(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    1,
                    20,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/identityCard/list"
                )!!
            }
        }

    }

    suspend fun getFingerprintList(lockId: Int): FingerprintList {
        try {
            if (BuildConfig.IS_OPENTT_URL_CALL) {
                return apiRequest {
                    rudoApi.getFingerprintListOpenTTApi(
                        BuildConfig.CLIENT_ID,
                        appPrefrences.getAccessToken(),
                        lockId,
                        1,
                        20,
                        System.currentTimeMillis()
                    )!!
                }
            } else {
                return apiRequest {
                    rudoApi.getFingerprintList(
                        BuildConfig.CLIENT_ID,
                        appPrefrences.getAccessToken(),
                        lockId,
                        1,
                        20,
                        System.currentTimeMillis(),
                        BuildConfig.REDIRECT_OPENTT_URI + "/v3/fingerprint/list"
                    )!!
                }
            }
        } catch (e: ApiException) {
            return throw e
        } catch (e: NoInternetException) {
            return throw e
        } catch (e: Exception) {
            return throw e
        }
    }


    suspend fun addIcCard(
        lockId: Int,
        cardNumber: String,
        cardName: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {

        return apiRequest {
            rudoApi.addICCard(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                cardNumber,
                cardName,
                startDate,
                endDate,
                1,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/identityCard/add"
            )!!
        }
    }

    suspend fun addIcCardOpenTTApi(
        lockId: Int,
        cardNumber: String,
        cardName: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {

        return apiRequest {
            rudoApi.addICCardOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                cardNumber,
                cardName,
                startDate,
                endDate,
                1,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun addFingerprint(
        lockId: Int,
        fingerprintNumber: String,
        fingerprintName: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {

        return apiRequest {
            rudoApi.addFingerprint(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                fingerprintNumber,
                fingerprintName,
                startDate,
                endDate,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/fingerprint/add"
            )!!
        }
    }

    suspend fun addFingerprintOpenTT(
        lockId: Int,
        cardNumber: String,
        cardName: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {

        return apiRequest {
            rudoApi.addFingerprintOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                cardNumber,
                cardName,
                startDate,
                endDate,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun uploadRecodes(lockId: Int, logs: String): ApiResponseModel {

        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.unlockRecordsUploadOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    logs,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.unlockRecordsUpload(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    logs,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_URI,
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/lockRecord/upload"
                )!!
            }
        }
    }


    suspend fun getUnloackRecods(lockId: Int): RecordListModel {
        val result = apiRequest {
            rudoApi.getUnlockRecords(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                0,
                0,
                1,
                100,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/lockRecord/list"
            )!!
        }
        result.let {
            it.list.forEach {
                if (it.username != null) {
                    if (it.username!!.contains("_")) {
                        it.username = it.username!!.split("_")[1]
                    }
                }
            }
        }
        return result
    }

    suspend fun getUnloackRecodsOpenTTApi(lockId: Int): RecordListModel {
        val result = apiRequest {
            rudoApi.getUnlockRecordsOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                0,
                0,
                1,
                100,
                System.currentTimeMillis()
            )!!
        }

        result.let {
            it.list?.forEach {
                if (it.username != null) {
                    if (it.username!!.contains("_")) {
                        it.username = it.username!!.split("_")[1]
                    }
                }
            }
        }
        return result
    }

    suspend fun clearIcCard(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.clearIccard(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/identityCard/clear"
            )!!
        }
    }

    suspend fun clearIcCardOpenTT(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.clearIccardOpenTT(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun clearFingerprint(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.clearIccard(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/fingerprint/clear"
            )!!
        }
    }

    suspend fun clearFingerprintOpenTT(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.clearIccardOpenTT(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockId,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun deletUser(): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.deletUserOpenTTApi(
                    BuildConfig.CLIENT_ID,
                    BuildConfig.CLIENT_SECRET,
                    Constants.PREFIX_USERNAME + appPrefrences.getUserInfo()!!,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.deletUser(
                    BuildConfig.CLIENT_ID,
                    BuildConfig.CLIENT_SECRET,
                    Constants.PREFIX_USERNAME + appPrefrences.getUserInfo()!!,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/user/delete"
                )!!
            }
        }

    }


    suspend fun deleteLock(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.deleteLock(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken()!!,
                lockId,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/delete"
            )!!
        }
    }

    suspend fun deleteLockOpenTT(lockId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.deleteLockOpenTT(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken()!!,
                lockId,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun deleteIcCard(lockId: Int, cardId: Int,deleteType:Int): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL){
            return apiRequest {
                rudoApi.deleteIcCardOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    lockId,
                    cardId,
                    deleteType,
                    System.currentTimeMillis()
                )!!
            }
        }else {
            return apiRequest {
                rudoApi.deleteIcCard(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    lockId,
                    cardId,
                    deleteType,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/identityCard/delete"
                )!!
            }
        }
    }



    suspend fun deleteFingerprint(lockId: Int, fingerprintId: Int,deleteType: Int): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL){
            return apiRequest {
                rudoApi.deleteFingerprintOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    lockId,
                    fingerprintId,
                    deleteType,
                    System.currentTimeMillis()
                )!!
            }
        }else{
            return apiRequest {
                rudoApi.deleteFingerprint(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    lockId,
                    fingerprintId,
                    deleteType,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/fingerprint/delete"
                )!!
            }
        }
    }


    suspend fun deleteeAccessKey(keyId: Int): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.deleteeAccessKeyOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    keyId,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.deleteeAccessKey(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    keyId,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/key/delete"
                )!!
            }
        }

    }

    suspend fun deletePasscode(lockId: Int, passcodeId: Int,deleteType:Int): ApiResponseModel {

        if(BuildConfig.IS_OPENTT_URL_CALL){
            return apiRequest {
                rudoApi.deleteePasscodeOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    lockId,
                    passcodeId,
                    deleteType,
                    System.currentTimeMillis()
                )!!
            }
        }else{
            return apiRequest {
                rudoApi.deleteePasscode(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    lockId,
                    passcodeId,
                    deleteType,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/keyboardPwd/delete"
                )!!
            }
        }

    }


    suspend fun authorizeEAccessOpenTT(lockId: Int, keyId: Int): ApiResponseModel {
        return apiRequest {
            rudoApi.authorizeEAccessOpenTT(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken()!!,
                lockId,
                keyId,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun addCustomPasscode(
        lockId: Int,
        customPasscode: String,
        name: String,
        startDate: Long,
        endDate: Long
    ): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.addCustomPasscodeOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    lockId,
                    customPasscode,
                    name,
                    startDate,
                    endDate,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return return apiRequest {
                rudoApi.addCustomPasscode(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken()!!,
                    lockId,
                    customPasscode,
                    name,
                    startDate,
                    endDate,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/keyboardPwd/add"
                )!!
            }
        }
    }


    suspend fun uploadBatteryStatus(lockId: Int, batteryStatus: Int): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.uploadBatteryStatusOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    batteryStatus,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.uploadBatteryStatus(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    batteryStatus,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/updateElectricQuantity"
                )!!
            }
        }
    }

    suspend fun lockViaGateway(lockId: Int): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.lockViaGatewayOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.lockViaGateway(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/lock"
                )!!
            }
        }
    }

    suspend fun unLockViaGateway(lockId: Int): ApiResponseModel {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.unLockViaGatewayOpenTT(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.unLockViaGateway(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/unlock"
                )!!
            }
        }
    }

    suspend fun getLockStatus(lockId: Int): LockStatus {
        if (BuildConfig.IS_OPENTT_URL_CALL) {
            return apiRequest {
                rudoApi.getLockStatus(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis()
                )!!
            }
        } else {
            return apiRequest {
                rudoApi.getLockStatus(
                    BuildConfig.CLIENT_ID,
                    appPrefrences.getAccessToken(),
                    lockId,
                    System.currentTimeMillis(),
                    BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/queryOpenState"
                )!!
            }
        }
    }
}