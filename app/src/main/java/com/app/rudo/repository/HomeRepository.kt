package com.app.rudo.repository

import com.app.rudo.BuildConfig
import com.app.rudo.model.InitLockModel
import com.app.rudo.model.LockKeyListMode
import com.app.rudo.model.locklist.GetLockList
import com.app.rudo.network.ApiRequest
import com.app.rudo.network.api.RudoApi
import com.app.rudo.utils.ApiException
import com.app.rudo.utils.AppPrefrences
import com.app.rudo.utils.NoInternetException
import com.app.rudo.utils.ServerException
import java.net.URLDecoder

/*
// Created by Satyabrata Bhuyan on 02-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class HomeRepository(
    private val rudoApi: RudoApi,
    private val appPrefrences: AppPrefrences
) : ApiRequest() {
    suspend fun getLockList(): GetLockList {
        try {
            if (BuildConfig.IS_OPENTT_URL_CALL) {
                val result = apiRequest {
                    rudoApi.getLockListFromOpenTTApi(
                        BuildConfig.CLIENT_ID,
                        appPrefrences.getAccessToken(),
                        1,
                        20,
                        System.currentTimeMillis()
                    )!!
                }
                result?.let {
                    result.list?.filter { it.lockAlias!!.isNotEmpty() }?.forEach {
                        val decoded: String = URLDecoder.decode(it.lockAlias, "UTF-8")
                        it.lockAlias = decoded
                    }
                }

                return result
            } else {
                val result = apiRequest {
                    rudoApi.getLockList(
                        BuildConfig.CLIENT_ID,
                        appPrefrences.getAccessToken(),
                        1,
                        20,
                        System.currentTimeMillis(),
                        BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/list"
                    )!!
                }
                result?.let {
                    result.list?.forEach {
                        val decoded: String = URLDecoder.decode(it.lockAlias, "UTF-8")
                        it.lockAlias = decoded
                    }
                }
                return result
            }
        } catch (e: NoInternetException) {
           // throw e
            var _getLockList = GetLockList(null,null,null,null,null,e.message,"null")
            return _getLockList
        } catch (e: ApiException) {
           // throw e
            var _getLockList = GetLockList(null,null,null,null,null,e.message,"null")
            return _getLockList
        } catch (e: ServerException) {
           // throw e
            var _getLockList = GetLockList(null,null,null,null,null,e.message,"null")
            return _getLockList
        } catch (e: Exception) {
           // throw e
            var _getLockList = GetLockList(null,null,null,null,null,e.message,"null")
            return _getLockList
        }

    }

    suspend fun initLock(
        lockData: String,
        aliasName: String
    ): InitLockModel {
        return apiRequest {
            rudoApi.lockInit(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockData,
                aliasName,
                System.currentTimeMillis(),
                BuildConfig.REDIRECT_OPENTT_URI + "/v3/lock/initialize"
            )!!
        }
    }

    suspend fun initLockOpenTTApi(
        lockData: String,
        aliasName: String
    ): InitLockModel {
        return apiRequest {
            rudoApi.lockInitOpenTTApi(
                BuildConfig.CLIENT_ID,
                appPrefrences.getAccessToken(),
                lockData,
                aliasName,
                System.currentTimeMillis()
            )!!
        }
    }

    suspend fun getEkeyList(): LockKeyListMode {
        try {

            if (BuildConfig.IS_OPENTT_URL_CALL) {
                val result = apiRequest {
                    rudoApi.getEkeyListOpenTT(
                        BuildConfig.CLIENT_ID,
                        appPrefrences.getAccessToken(),
                        1,
                        20,
                        System.currentTimeMillis()
                    )!!
                }
                result?.let {
                    result.list?.filter { it.lockAlias!!.isNotEmpty() }?.forEach {
                        val decoded: String = URLDecoder.decode(it.lockAlias, "UTF-8")
                        it.lockAlias = decoded
                    }
                }
                return result
            } else {
                val result = apiRequest {
                    rudoApi.getEkeyList(
                        BuildConfig.CLIENT_ID,
                        appPrefrences.getAccessToken(),
                        1,
                        20,
                        System.currentTimeMillis(),
                        BuildConfig.REDIRECT_OPENTT_URI + "/v3/key/list"
                    )!!
                }
                result?.let {
                    result.list?.filter { it.lockAlias!!.isNotEmpty() }?.forEach {
                        val decoded: String = URLDecoder.decode(it.lockAlias, "UTF-8")
                        it.lockAlias = decoded
                    }
                }
                return result
            }
        } catch (e: NoInternetException) {
           // throw e
            val _getEkey = LockKeyListMode(null,null,null,null,null,e.message)
            return _getEkey
        } catch (e: ApiException) {
            //throw e
            val _getEkey = LockKeyListMode(null,null,null,null,null,e.message)
            return _getEkey
        } catch (e: ServerException) {
           // throw e
            val _getEkey = LockKeyListMode(null,null,null,null,null,e.message)
            return _getEkey
        } catch (e: Exception) {
            //throw e
            val _getEkey = LockKeyListMode(null,null,null,null,null,e.message)
            return _getEkey
        }
    }
}