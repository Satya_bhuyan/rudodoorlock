package com.app.rudo.model.lockdetails

import java.io.Serializable

/*
// Created by Satyabrata Bhuyan on 05-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class LockDetails(
    val lockId:Int,
    val lockName:String,
    var lockAlias:String,
    val lockMac:String,
    val lockKey:String,	//The key data which will be used to unlock
    val lockFlagPos:Int, //	The flag which will be used to check the validity of the ekey
    val adminPwd:String,//	admin code, which only belongs to the admin ekey, will be used to verify the admin permission.
    val noKeyPwd:String,//	Super passcode, which only belongs to the admin ekey, can be entered on the keypad to unlock
    val deletePwd:String,//	Erasing passcode,which belongs to old locks, has been abandoned. Please don't use it.
    val aesKeyStr:String,//AES encryption key
    val lockVersion:LockVersionModel,//	Lock version
    val keyboardPwdVersion:Int,//	Passcode version: 0、1、2、3、4
    val electricQuantity:Int,//	Lock battery
    val specialValue:Int,//	characteristic value. it is used to indicate what kinds of feature do a lock support.
    val timezoneRawOffset:Long,//	The offset between your time zone and UTC, in millisecond
    val modelNum:String,//	Product model
    val hardwareRevision:String,//	Hardware version
    val firmwareRevision:String,//	Firmware version
    val date:Long,	//Lock init time
    val errcode :Int?,
    val errmsg :String?,
    val description:String?,
    var hasGateway:Int?     // Is the lock binded to gateway:1-yes, 0-noal

): Serializable



