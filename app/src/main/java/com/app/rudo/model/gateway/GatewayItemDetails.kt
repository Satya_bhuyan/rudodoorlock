package com.app.rudo.model.gateway

import java.io.Serializable

/*
// Created by Satyabrata Bhuyan on 20-05-2021.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class GatewayItemDetails(
    val gatewayId: Int?,
    val gatewayMac: String?,
    val isOnline: Int?,
    val lockNum: Int?,
    val gatewayName :String?,
    val networkName :String?,
    val gatewayVersion:Int?,
    var status:String?

):Serializable