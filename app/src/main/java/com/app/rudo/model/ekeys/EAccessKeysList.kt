package com.app.rudo.model.ekeys

import java.io.Serializable

data class EAccessKeysList(
    val list: List<EAccessKey>?,
    val pageNo: Int,
    val pageSize: Int,
    val pages: Int,
    val total: Int,
    val errcode :Int?,
    val errmsg :String?,
    val description:String?
):Serializable