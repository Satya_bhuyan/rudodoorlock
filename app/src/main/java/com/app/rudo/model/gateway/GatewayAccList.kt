package com.app.rudo.model.gateway

data class GatewayAccList(
    val list: List<AccList>,
    var errcode: Int? = null,
    var errmsg: String? = null,
    var description:String? = null

)