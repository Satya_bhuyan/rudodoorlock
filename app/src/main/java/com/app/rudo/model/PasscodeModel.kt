package com.app.rudo.model

/*
// Created by Satyabrata Bhuyan on 08-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class PasscodeModel(
    val keyboardPwdId:Int?,
    val keyboardPwd:String?,
    val errcode: Int?,
    val errmsg: String?,
    val description:String?
)

