package com.app.rudo.model.locklist

data class LockListModel(
    val list: List<Lock>?,
    val description:String?,
    val errcode: Int?,
    val errmsg: String?
)