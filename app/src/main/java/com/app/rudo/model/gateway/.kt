package com.app.rudo.model.gateway

data class AccList(
    val gatewayId: Int?,
    val gatewayMac: String?,
    val isOnline: Int?,
    val lockNum: Int?,
    val gatewayName :String?,
    val networkName :String?,
    val gatewayVersion:Int?,
    var status:String?

)