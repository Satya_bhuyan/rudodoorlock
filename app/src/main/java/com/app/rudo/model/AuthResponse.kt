package com.app.rudo.model

import java.io.Serializable

data class AuthResponse(
    val access_token: String?,
    val uid: Int?,
    val expires_in: Int?,
    val scope: String?,
    val refresh_token: String?,
    val errcode :Int?,
    val errmsg :String?,
    val description:String?,
    var md5Pwd:String?
):Serializable