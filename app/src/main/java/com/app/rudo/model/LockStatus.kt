package com.app.rudo.model

/*
// Created by Satyabrata Bhuyan on 10-06-2021.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class LockStatus(
    val status: Int?,
    val errcode: Int?,
    val errmsg: String?,
    val state :Int?,
    val sensorState:Int?
)
