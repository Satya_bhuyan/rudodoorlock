package com.app.rudo.model

data class ApiResponseModel(
    val errcode: Int?,
    val errmsg: String?,
    val keyId: Int?,
    val cardId: Int?,
    val description:String?,
    val fingerprintId:Int,
    val keyboardPwdId :Int?,
    val Status : String?,
    val Details : String?,
    val gatewayId : Int?,
    val OTP: Int?,
    val error:String?
)
