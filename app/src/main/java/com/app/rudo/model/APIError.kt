package com.app.rudo.model

data class APIError(
    val error: String,
    val message: String,
    val path: String,
    val status: Int,
    val timestamp: String
)