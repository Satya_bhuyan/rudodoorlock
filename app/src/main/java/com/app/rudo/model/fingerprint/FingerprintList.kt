package com.app.rudo.model.fingerprint

/*
// Created by Satyabrata Bhuyan on 11-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

class FingerprintList(
    val list:List<FingerprintData>?,
    val pageNo: Int?,
    val pageSize: Int?,
    val pages: Int?,
    val total: Int?,
    val errcode :Int?,
    val errmsg :String?,
    val description:String?
)
