package com.app.rudo.model.locklist

import java.io.Serializable

/*
// Created by Satyabrata Bhuyan on 12-06-2021.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/
data class GetLockList(
    var list: MutableList<LockDetailsData>?,
    val total: Int?,
    val page: Int?,
    val pageNo:Int?,
    val pageSize:Int?,
    val errorMessage:String?,
    val errmsg:String?
):Serializable