package com.app.rudo.model.fingerprint

/*
// Created by Satyabrata Bhuyan on 11-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class FingerprintData(
    var fingerprintId: Int?,
    var lockId: Int?,
    var fingerprintNumber: String?,
    var fingerprintName: String?,
    var startDate: Long?,
    var endDate: Long?,
    var createDate: Long?,
    val status:	Int
)