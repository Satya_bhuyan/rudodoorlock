package com.app.rudo.model

/*
// Created by Satyabrata Bhuyan on 10-07-2020.
// Company  Yutu Electronics PVT LTD.
// E_Mail   s.bhuyan0037@gmail.com
*/

data class LockKey (
    val date: Long,
    val endDate: Long,
    val keyId: Int,
    val keyStatus: String,
    val lockId: Int,
    val openid: Int,
    val remarks: String,
    val startDate: Long,
    var username: String,
    val keyName:String?,
    val senderUsername:String?,
    val keyRight:Int?

)
